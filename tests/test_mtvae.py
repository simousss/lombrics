def test_mtvae(fusion='early'):
    
    name='mtvae' 
    
    
    mlpenc = {
            'archi':[32,16],
            'reg':None,
            'activation':['relu']*2
    }
    
    mlpfe = {
            'archi':[],
            'reg':None,
            'activation':None
    }    
    
    embed_config = {
        'features':[('X5',5,2),('X6',15,3)],
        'reg':None
    }    
    
    fe_config={
        'num_vars':['X1','X2','X3'],
        'cat_vars':['X4','X5'],
        'embed':embed_config,
        'fe': mlpfe
    }    
    
    encoder_config={
        'mlp':mlpenc,
        'latentdim':5,
        'beta':1,
        'nsamples':10,

    } 
    
    mlpdec={
        'archi':[],
        'reg':None,
        'activation': None
    }
    
    task_config= lambda dist, m: {
        'ntasks':m,
        'dist':dist,
        'activation':None,
        'mlp':mlpdec
    }
    
    
    mt_configs =[
        task_config('categorical', 5),
        task_config('poisson', 1),
        task_config('bernoulli', 10)
    ]
    
    tnames = ['A','B','C']      
    
    
    decoder_config = {'type':'deterministic','config':mt_configs, 'tnames':tnames}
            
    model = MTVAE(fe_config, encoder_config, decoder_config, name)
    
    return model
    
model = test_mtvae()