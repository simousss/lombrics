def test_msdm():
    problem = {
        'name':'Lombrics',
        'ntaxa':80,
        'numvars':['A','B','C'],
        'catvars':['X','Y'],
        'catclasses':{'X':np.arange(5),'Y':np.arange(15)}
    }

    archi = {
        'embed' : dict(features=[('X',5,['X%d'%i for i in range(5)],2),('Y',15,['Y%d'%i for i in range(15)],3)], reg=None),
        'fe': dict(archi = [8,4] , reg= None , activation = ['relu']*2),
        'bias':'zeros',
        'dist' : 'probit',
        'mode': 'd'
    }

    msdm = MSDModel(problem, archi)

    inputs = [np.random.normal(0,1,(1000,3)) , np.random.choice(5,size=(1000,)), np.random.choice(15,size=(1000,))]
    
    return msdm(inputs).shape, msdm

#test_msdm()


