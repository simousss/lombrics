def test_cat2vec(bs=(32,8,8)):
    embed_config = {
        'features':[('A',5,2),('B',15,3),('C',32,4)],
        'reg':None
    }
    
    layer = Cat2Vec(embed_config,'test')
    
    a=np.random.choice(5,size=(bs))
    b=np.random.choice(15,size=(bs))
    c=np.random.choice(32,size=(bs))
    
    assert (layer([a,b,c]).shape==bs+(9,))

#test_cat2vec(bs=(100,1))

def test_mlp():
    mlp_config={
        'archi': [], #[64,0.5,32,0.8,16],
        'reg':None,
        'activation':None #['relu']*5
    }
    
    mlp=MLP(mlp_config,'test')
    
    a = np.random.normal(loc=0,scale=1,size=(32,5))
    
    return mlp(a)-a
    
#test_mlp()


def test_gaussian_encoder():
    mlp = {
            'archi':[32,16],
            'reg':None,
            'activation':['relu']*2
    }
    
    encoder_config={
        'mlp':mlp,
        'latentdim':5,
        'beta':1,
        'nsamples':10,

    }
    
    ge = GaussianEncoder(encoder_config,'model')
    
    x = np.random.choice(2,size=(128,20))
    
    out=ge(x).sample(10)
    
    expected = (10,128,5)
    
    assert out.shape==expected

#test_gaussian_encoder()

def test_decoder(dist):
    
    name='%s_decoder'%dist
    mlp={
        'archi':[64,0.5,32,0.8,16],
        'reg':None,
        'activation':['relu']*5
    }
    decoder_config={
        'ntasks':10,
        'dist':dist,
        'activation':None,
        'mlp':mlp
    }
    
    x= np.random.normal(0,1,(64,5))
    dec=Decoder(decoder_config, name)
    y=dec(x)
    
    return y

#test_decoder('probit').shape
#test_decoder('poisson').shape
#test_decoder('apoisson').shape


def test_pdecoder(dist):
    
    name='%s_pdecoder'%dist
    mlp={
        'archi':[64,0.5,32,0.8,16],
        'reg':None,
        'activation':['relu']*5
    }
    decoder_config={
        'ntasks':10,
        'dist':dist,
        'activation':None,
        'mlp':mlp
    }
    
    x= np.random.normal(0,1,(64,5))
    dec=ProbaDecoder(decoder_config, name)
    y=dec(x)
    
    return y

#test_pdecoder('independent'), test_pdecoder('poisson'), test_pdecoder('apoisson'), 
#test_pdecoder('categorical'), test_pdecoder('bernoulli')


def test_multidecoder():
    
    name='multidecoder'
    mlp={
        'archi':[64,0.5,32,0.8,16],
        'reg':None,
        'activation':['relu']*5
    }
    decoder_config= lambda dist, m: {
        'ntasks':m,
        'dist':dist,
        'activation':None,
        'mlp':mlp
    }
    
    decoder_configs =[
        decoder_config('categorical', 5),
        decoder_config('poisson', 1),
        decoder_config('bernoulli', 10)
    ]
    
    tnames = ['A','B','C']
    
    x= np.random.normal(0,1,(64,5))
    dec=MultiDecoder(decoder_configs, tnames, name)
    y=dec(x)
    
    return [out.shape for out in y]

#test_multidecoder()

def test_multipdecoder():
    
    name='multidecoder'
    mlp={
        'archi':[64,0.5,32,0.8,16],
        'reg':None,
        'activation':['relu']*5
    }
    decoder_config= lambda dist, m: {
        'ntasks':m,
        'dist':dist,
        'activation':None,
        'mlp':mlp
    }
    
    
    decoder_configs =[
        decoder_config('categorical', 5),
        decoder_config('poisson', 1),
        decoder_config('bernoulli', 10)
    ]
    
    tnames = ['A','B','C']
    
    x= np.random.normal(0,1,(64,5))
    dec=ProbaMultiDecoder(decoder_configs, tnames, name)
    y=dec(x)
    
    return [out for out in y]

#test_multipdecoder()