import numpy as np
import pandas as pd
from scipy.special import logit

def test_msdm():
    problem = {
        'name':'Lombrics',
        'ntaxa':80,
        'numvars':['A','B','C'],
        'catvars':['X','Y'],
        'catclasses':{'X':np.arange(5),'Y':np.arange(15)}
    }

    archi = {
        'embed' : dict(features=[('X',5,2,['X%d'%i for i in range(5)]),('Y',15,3,['Y%d'%i for i in range(15)])], reg=None),
        'fe': dict(archi = [8,4] , reg= None , activation = ['relu']*2),
        'bias':'zeros',
        'dist' : 'probit',
        'mode': 'd'
    }

    msdm = MSDModel(problem, archi)


    x = [np.random.normal(0,1,(1000,3)) , np.random.choice(5,size=(1000,)), np.random.choice(15,size=(1000,))]
    y = np.random.choice(2,size=(1000,80),p=[0.5,0.5])

    msdm.compile(optimizer='adam', loss='binary_crossentropy')
    cbk1=InspectNN(period=2, model=msdm)
    cbk2 = MlRankMetrics(valid_dataset=(x,y))
    history = msdm.fit(x=x, y=y, verbose=1, epochs=5, callbacks=[cbk1, cbk2])
    
    return msdm


def test_losses():
    ypredp = tfp.distributions.Bernoulli(probs=np.array([[0.2,0.5,0.7,0.1]]*3))
    ypred = np.array([[0.2,0.5,0.7,0.1]]*3)
    ylogit = logit(ypred)
    ytrue = np.array([[0,1,1,1]]*3)

    w= np.ones(4)

    nll_ = msdm_losses.get('nll')(w)
    bce_ = msdm_losses.get('bce')(w)
    unbce_ = msdm_losses.get('cwbce')(w)
    #fl0_ = stable_focal_loss(gamma=0.0, alpha=1.0)
    pkgfl0_ = BinaryFocalLoss(gamma = 0.0)
#     tfafl0_ = SigmoidFocalCrossEntropy(gamma=0.0, alpha=1.0, reduction=Reduction.SUM, from_logits=True)

    rich_bce = msdm_losses.get('rich')(bce_)
    rich_unbce = msdm_losses.get('rich')(unbce_)
    rich_fl0 = msdm_losses.get('rich')(pkgfl0_)

    print('NegLL: %f'%nll_(ytrue, ypredp)) 
    print('BinCE: %f'%bce_(ytrue, ypred)) 
    print('UnwBCE: %f'%unbce_(ytrue, ypred)) 
    #print('FL0: %f'%fl0_(ytrue, ylogit)) 
    print('Pkg-FL0: %f'%pkgfl0_(ytrue, ypred))
    print('Richness+BinCE: %f'%rich_bce(ytrue, ypred)) 
    print('Richness+UnwBCE: %f'%rich_unbce(ytrue, ypred)) 
    print('Richness+FL0: %f'%rich_fl0(ytrue, ypred)) 
    
test_losses()

