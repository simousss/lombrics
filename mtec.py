import tensorflow as tf
import tensorflow_probability as tfp 

tfk = tf.keras
tfkl = tfk.layers
tfkr = tfk.regularizers
tfd = tfp.distributions
tfpl = tfp.layers


def mtvae_loss(y_true,y_pred):
    return -y_pred.log_prob(y_true)

def rich_loss(y_true, y_pred):    
    negll=-tf.reduce_mean(y_pred.log_prob(y_true))
    
    y_true = tf.cast(y_true, dtype=tf.float32)
    rich_true = tf.reduce_sum(y_true,axis=-1)
    rich_pred = tf.reduce_sum(y_pred,axis=-1)
    richll=tf.reduce_sum(tf.math.exp(rich_true - rich_pred))
    
    return negll + richll


def dirich_multi_loss(y_true, y_pred):
    total = tf.reduce_sum(y_true, axis=-1)
    pred_dist = tfd.DirichletMultinomial(
                    total_count=total,
                    concentration=y_pred)
    
    return -pred_dist.log_prob(y_true)
    
    
class MTVAE(object):
    def __init__(self, name='mtvae', prob_config=None, archi_config=None, learn_config=None):
        self.name = name  ### name of the model, appended to all layer names
        self.prob_config = prob_config  ### all about the problem setting
        self.archi_config = archi_config  ### all about architecture 
        self.learn_config = learn_config    ### all about training hyperparameters, metrics and losses
        
        super(MTVAE, self).__init__()
    
    
    def create_mtvae_model(self):
        ### Problem setting
        num_vars = self.prob_config.get('num_vars')
        cat_vars = self.prob_config.get('cat_vars')
        ntasks = self.prob_config.get('ntasks')
        
        inputs = []
        feats = []
        
        if len(num_vars)>0:
            innum = tfkl.Input(shape = (len(num_vars),), name='%s_innum'%self.name, dtype=tf.float32)
            inputs+=[innum]
            feats+=[innum]
            
        if len(cat_vars)>0:
            incat = [
                tfkl.Input(shape=(1,), name='%s_incat_%s'%(self.name,c), dtype=tf.int32) for c in cat_vars
            ]
            
            vocabsize = self.archi_config.get('embedding').get('vocabsize')
            embsize = self.archi_config.get('embedding').get('embsize')
            embreg = self.archi_config.get('embedding').get('reg')
            
            emball = [
                tfkl.Embedding(input_dim = vocabsize[i], output_dim = embsize[i],embeddings_regularizer = embreg, 
                               name='%s_emb_%s'%(self.name,c)) (incat[i]) 
                for i, c in enumerate(cat_vars)
            ]
            
            embcat = tfkl.Concatenate()(emball)
            
            embcat = tf.squeeze(embcat,axis=-2)
            inputs+= incat
            feats+= [embcat]
        
        
        ### Concatenate raw features
        hidden = tfkl.Concatenate()(feats)
        
        ### Shared configuration across components
        kreg = self.archi_config.get('reg')
        hidden_act = self.archi_config.get('hidden_act')
        
        ### feature extraction part
        fe_archi = self.archi_config.get('fe')
        for i, nn in enumerate(fe_archi):
            hidden= tfk.Dense(units = nn, name='%s_dense_%d'%(self.name,i), 
                              activation = hidden_act, kernel_regularizer = kreg)(hidden)
        
        
        ### vae component
        vae_config = self.archi_config.get('vae')
        if vae_config is not None:
            latentdim = vae_config.get('latentdim')
            
            paramsize = tfpl.MultivariateNormalTriL.params_size(latentdim)
            encoder_archi = vae_config.get('encoder')+[paramsize]
            
            ## Input
            in_encoder = tfkl.Input(shape = (ntasks,), dtype = tf.float32, name='%s_in_encoder'%self.name)
            inputs+=[in_encoder]
            
            
            ## Prior
            prior = tfd.MultivariateNormalDiag(loc=tf.zeros(latentdim),scale_diag=tf.ones(latentdim))
            
            ## Encoder            
            ereg = vae_config.get('ereg')
            eact = vae_config.get('activation')
            beta = vae_config.get('beta')
            nsamples = vae_config.get('nsamples')
            
            latent = in_encoder
            for i, nn in enumerate(encoder_archi):
                latent = tfkl.Dense(units = nn, name='%s_encoder_%d' %(self.name, i), activation=eact,
                                    dtype=tf.float32, kernel_regularizer = ereg)(latent)
                
            ## Define the regularizer
            kl_regularizer = tfpl.KLDivergenceRegularizer(
                prior,
                weight=beta,
                use_exact_kl=True,
                test_points_fn=lambda q: q.sample(nsamples),
                test_points_reduce_axis=None
            )
            
            ## Parameterize the approximate posterior
            latentsample = tfpl.MultivariateNormalTriL(event_size = latentdim, 
                                                       name='%s_latentsample'%self.name,
                                                       activity_regularizer=kl_regularizer)(latent)
            
            ## Combine latent factor and learnt features
            hidden = tfkl.Concatenate()([latentsample,hidden])
            
        
        ### prediction component
        pred_config = self.archi_config.get('prediction')
        
        ### Here, we adapt the output dependending on whether it's a binary or count distribution
        dist = pred_config.get('distribution')
        if dist=='binary':
            out_params = tfkl.Dense(units=tfpl.IndependentBernoulli.params_size(ntasks), dtype=tf.float32,
                                    name='%s_outparams'%self.name,
                                    kernel_regularizer=kreg)(hidden)

            out_pred = tfpl.IndependentBernoulli(event_shape=ntasks,name='%s_prediction'%self.name)(out_params)
            
        elif dist=='count':
            out_params = tfkl.Dense(units=tfpl.IndependentPoisson.params_size(ntasks), dtype=tf.float32,
                                    activation=pred_config.get('activation'),
                                    name='%s_outparams'%self.name,
                                    kernel_regularizer=kreg)(hidden)

            out_pred = tfpl.IndependentPoisson(event_shape=ntasks,name='%s_prediction'%self.name)(out_params)
        
        elif dist=='relative':
            out_pred = tfkl.Dense(units=ntasks, dtype=tf.float32,
                                    activation='softmax',
                                    name='%s_outparams'%self.name,
                                    kernel_regularizer=kreg)(hidden)
            
            
#             out_pred = tfkl.Lambda(lambda t: tf.reduce_sum(t, axis=-1))(out_params)
#             out_pred = tfpl.DistributionLambda(
#                 make_distribution_fn=lambda t: tfd.DirichletMultinomial(
#                     total_count=tf.reduce_sum(t, axis=-1),
#                     concentration=t),
                
#                 convert_to_tensor_fn=tfp.distributions.Distribution.sample,
#                 name='%s_prediction'%self.name)(out_params)
            
#             out_rich = tfkl.Lambda(lambda t: tf.reduce_sum(t,axis=-1))(out_pa)
            
#             out_pred = [out_pa, out_rich]
            
        else:
            print('No distribution specified ! We will use the Normal by default for independent continuous outputs. ')
            out_params = tfkl.Dense(units=tfpl.MultivariateNormalDiag.params_size(ntasks), dtype=tf.float32,
                                    name='%s_outparams'%self.name,
                                    activation=pred_config.get('activation'),
                                    kernel_regularizer=kreg)(hidden)

            out_pred = tfpl.MultivariateNormalDiag(event_shape=ntasks,name='%s_prediction'%self.name)(out_params)
        
        ### This is the full multi-task VAE
        self.mtvae = tfk.Model(inputs=inputs, outputs=out_pred)
        
        ### TODO: This is the multi-task model used for prediction
        
        
        return self.mtvae
        
        
    def train_mtvae_model(self, trainset, validset):
        return None
        
        
    def evaluate_mtvae_model(self, testset):
        return None
        
    def predict_mtvae_model(self, newdata):
        return None
    