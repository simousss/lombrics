╓─
╨а
B
AssignVariableOp
resource
value"dtype"
dtypetypeИ
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(И

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetypeИ
е
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	И
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0И
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
╛
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring И
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
Ц
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 И"serve*2.5.02v2.5.0-rc3-213-ga4dfb8d1a718┘╫
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
Ў
Mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*^
shared_nameOMmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings
я
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Read/ReadVariableOpReadVariableOpMmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings*
_output_shapes

:*
dtype0
Ї
Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*]
shared_nameNLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings
э
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Read/ReadVariableOpReadVariableOpLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings*
_output_shapes

:*
dtype0
Ї
Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*]
shared_nameNLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings
э
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Read/ReadVariableOpReadVariableOpLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings*
_output_shapes

:*
dtype0
А
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*c
shared_nameTRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings
∙
fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Read/ReadVariableOpReadVariableOpRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings*
_output_shapes

:
*
dtype0
■
Qmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*b
shared_nameSQmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings
ў
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Read/ReadVariableOpReadVariableOpQmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings*
_output_shapes

:
*
dtype0
ъ
Gmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*X
shared_nameIGmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel
у
[msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/Read/ReadVariableOpReadVariableOpGmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel*
_output_shapes

:3M*
dtype0
т
Emsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*V
shared_nameGEmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias
█
Ymsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/Read/ReadVariableOpReadVariableOpEmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias*
_output_shapes
:M*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
t
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametrue_positives
m
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes
:*
dtype0
v
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_positives
o
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes
:*
dtype0
x
true_positives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nametrue_positives_1
q
$true_positives_1/Read/ReadVariableOpReadVariableOptrue_positives_1*
_output_shapes
:*
dtype0
v
false_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_negatives
o
#false_negatives/Read/ReadVariableOpReadVariableOpfalse_negatives*
_output_shapes
:*
dtype0
}
true_positives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_positives_2
v
$true_positives_2/Read/ReadVariableOpReadVariableOptrue_positives_2*
_output_shapes
:	╚M*
dtype0
y
true_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*
shared_nametrue_negatives
r
"true_negatives/Read/ReadVariableOpReadVariableOptrue_negatives*
_output_shapes
:	╚M*
dtype0

false_positives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_positives_1
x
%false_positives_1/Read/ReadVariableOpReadVariableOpfalse_positives_1*
_output_shapes
:	╚M*
dtype0

false_negatives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_negatives_1
x
%false_negatives_1/Read/ReadVariableOpReadVariableOpfalse_negatives_1*
_output_shapes
:	╚M*
dtype0
}
true_positives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_positives_3
v
$true_positives_3/Read/ReadVariableOpReadVariableOptrue_positives_3*
_output_shapes
:	╚M*
dtype0
}
true_negatives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_negatives_1
v
$true_negatives_1/Read/ReadVariableOpReadVariableOptrue_negatives_1*
_output_shapes
:	╚M*
dtype0

false_positives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_positives_2
x
%false_positives_2/Read/ReadVariableOpReadVariableOpfalse_positives_2*
_output_shapes
:	╚M*
dtype0

false_negatives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_negatives_2
x
%false_negatives_2/Read/ReadVariableOpReadVariableOpfalse_negatives_2*
_output_shapes
:	╚M*
dtype0
}
true_positives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_positives_4
v
$true_positives_4/Read/ReadVariableOpReadVariableOptrue_positives_4*
_output_shapes
:	╚M*
dtype0
}
true_negatives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_negatives_2
v
$true_negatives_2/Read/ReadVariableOpReadVariableOptrue_negatives_2*
_output_shapes
:	╚M*
dtype0

false_positives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_positives_3
x
%false_positives_3/Read/ReadVariableOpReadVariableOpfalse_positives_3*
_output_shapes
:	╚M*
dtype0

false_negatives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_negatives_3
x
%false_negatives_3/Read/ReadVariableOpReadVariableOpfalse_negatives_3*
_output_shapes
:	╚M*
dtype0
}
true_positives_5VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_positives_5
v
$true_positives_5/Read/ReadVariableOpReadVariableOptrue_positives_5*
_output_shapes
:	╚M*
dtype0
}
true_negatives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*!
shared_nametrue_negatives_3
v
$true_negatives_3/Read/ReadVariableOpReadVariableOptrue_negatives_3*
_output_shapes
:	╚M*
dtype0

false_positives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_positives_4
x
%false_positives_4/Read/ReadVariableOpReadVariableOpfalse_positives_4*
_output_shapes
:	╚M*
dtype0

false_negatives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	╚M*"
shared_namefalse_negatives_4
x
%false_negatives_4/Read/ReadVariableOpReadVariableOpfalse_negatives_4*
_output_shapes
:	╚M*
dtype0
Д
TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*e
shared_nameVTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/m
¤
hAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/m/Read/ReadVariableOpReadVariableOpTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/m*
_output_shapes

:*
dtype0
В
SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*d
shared_nameUSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/m
√
gAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/m/Read/ReadVariableOpReadVariableOpSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/m*
_output_shapes

:*
dtype0
В
SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*d
shared_nameUSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/m
√
gAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/m/Read/ReadVariableOpReadVariableOpSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/m*
_output_shapes

:*
dtype0
О
YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*j
shared_name[YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/m
З
mAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/m/Read/ReadVariableOpReadVariableOpYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/m*
_output_shapes

:
*
dtype0
М
XAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*i
shared_nameZXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/m
Е
lAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/m/Read/ReadVariableOpReadVariableOpXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/m*
_output_shapes

:
*
dtype0
°
NAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*_
shared_namePNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/m
ё
bAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/m/Read/ReadVariableOpReadVariableOpNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/m*
_output_shapes

:3M*
dtype0
Ё
LAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*]
shared_nameNLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/m
щ
`Adam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/m/Read/ReadVariableOpReadVariableOpLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/m*
_output_shapes
:M*
dtype0
Д
TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*e
shared_nameVTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/v
¤
hAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/v/Read/ReadVariableOpReadVariableOpTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/v*
_output_shapes

:*
dtype0
В
SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*d
shared_nameUSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/v
√
gAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/v/Read/ReadVariableOpReadVariableOpSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/v*
_output_shapes

:*
dtype0
В
SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*d
shared_nameUSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/v
√
gAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/v/Read/ReadVariableOpReadVariableOpSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/v*
_output_shapes

:*
dtype0
О
YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*j
shared_name[YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/v
З
mAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/v/Read/ReadVariableOpReadVariableOpYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/v*
_output_shapes

:
*
dtype0
М
XAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*i
shared_nameZXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/v
Е
lAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/v/Read/ReadVariableOpReadVariableOpXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/v*
_output_shapes

:
*
dtype0
°
NAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*_
shared_namePNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/v
ё
bAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/v/Read/ReadVariableOpReadVariableOpNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/v*
_output_shapes

:3M*
dtype0
Ё
LAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*]
shared_nameNLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v
щ
`Adam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v/Read/ReadVariableOpReadVariableOpLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v*
_output_shapes
:M*
dtype0

NoOpNoOp
Ъ]
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*╒\
value╦\B╚\ B┴\
╞
numvars
catvars
	archi
cat2vec
fconcat
fe
response
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
 
 

	embed
fe
p

config
embed_layers
regularization_losses
trainable_variables
	variables
	keras_api
R
regularization_losses
trainable_variables
	variables
	keras_api
j

config

layers
regularization_losses
trainable_variables
	variables
	keras_api
v

config
fe
 
prediction
!regularization_losses
"trainable_variables
#	variables
$	keras_api
╠
%iter

&beta_1

'beta_2
	(decay
)learning_rate*m╠+m═,m╬-m╧.m╨/m╤0m╥*v╙+v╘,v╒-v╓.v╫/v╪0v┘
 
1
*0
+1
,2
-3
.4
/5
06
1
*0
+1
,2
-3
.4
/5
06
н
	regularization_losses
1layer_metrics
2non_trainable_variables

3layers

trainable_variables
4metrics
	variables
5layer_regularization_losses
 

6features

	7archi
8
activation
#
90
:1
;2
<3
=4
 
#
*0
+1
,2
-3
.4
#
*0
+1
,2
-3
.4
н
regularization_losses
>layer_metrics
?non_trainable_variables

@layers
trainable_variables
Ametrics
	variables
Blayer_regularization_losses
 
 
 
н
regularization_losses
Clayer_metrics
Dnon_trainable_variables

Elayers
trainable_variables
Fmetrics
	variables
Glayer_regularization_losses
 
 
 
 
н
regularization_losses
Hlayer_metrics
Inon_trainable_variables

Jlayers
trainable_variables
Kmetrics
	variables
Llayer_regularization_losses
	
Mmlp
j

Mconfig

Nlayers
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
h

/kernel
0bias
Sregularization_losses
Ttrainable_variables
U	variables
V	keras_api
 

/0
01

/0
01
н
!regularization_losses
Wlayer_metrics
Xnon_trainable_variables

Ylayers
"trainable_variables
Zmetrics
#	variables
[layer_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
ФС
VARIABLE_VALUEMmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUE
УР
VARIABLE_VALUELmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUE
УР
VARIABLE_VALUELmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
ЩЦ
VARIABLE_VALUERmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
ШХ
VARIABLE_VALUEQmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
ОЛ
VARIABLE_VALUEGmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE
МЙ
VARIABLE_VALUEEmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUE
 
 

0
1
2
3
1
\0
]1
^2
_3
`4
a5
b6
 
#
c0
d1
e2
f3
g4
 
 
b
*
embeddings
hregularization_losses
itrainable_variables
j	variables
k	keras_api
b
+
embeddings
lregularization_losses
mtrainable_variables
n	variables
o	keras_api
b
,
embeddings
pregularization_losses
qtrainable_variables
r	variables
s	keras_api
b
-
embeddings
tregularization_losses
utrainable_variables
v	variables
w	keras_api
b
.
embeddings
xregularization_losses
ytrainable_variables
z	variables
{	keras_api
 
 
#
90
:1
;2
<3
=4
 
 
 
 
 
 
 
 
 
 
 
 

	|archi
}
activation
 
 
 
 
░
Oregularization_losses
~layer_metrics
non_trainable_variables
Аlayers
Ptrainable_variables
Бmetrics
Q	variables
 Вlayer_regularization_losses
 

/0
01

/0
01
▓
Sregularization_losses
Гlayer_metrics
Дnon_trainable_variables
Еlayers
Ttrainable_variables
Жmetrics
U	variables
 Зlayer_regularization_losses
 
 

0
 1
 
 
8

Иtotal

Йcount
К	variables
Л	keras_api
\
М
thresholds
Нtrue_positives
Оfalse_positives
П	variables
Р	keras_api
\
С
thresholds
Тtrue_positives
Уfalse_negatives
Ф	variables
Х	keras_api
v
Цtrue_positives
Чtrue_negatives
Шfalse_positives
Щfalse_negatives
Ъ	variables
Ы	keras_api
v
Ьtrue_positives
Эtrue_negatives
Юfalse_positives
Яfalse_negatives
а	variables
б	keras_api
v
вtrue_positives
гtrue_negatives
дfalse_positives
еfalse_negatives
ж	variables
з	keras_api
v
иtrue_positives
йtrue_negatives
кfalse_positives
лfalse_negatives
м	variables
н	keras_api

о3

п3

░3

▒3

▓3
 

*0

*0
▓
hregularization_losses
│layer_metrics
┤non_trainable_variables
╡layers
itrainable_variables
╢metrics
j	variables
 ╖layer_regularization_losses
 

+0

+0
▓
lregularization_losses
╕layer_metrics
╣non_trainable_variables
║layers
mtrainable_variables
╗metrics
n	variables
 ╝layer_regularization_losses
 

,0

,0
▓
pregularization_losses
╜layer_metrics
╛non_trainable_variables
┐layers
qtrainable_variables
└metrics
r	variables
 ┴layer_regularization_losses
 

-0

-0
▓
tregularization_losses
┬layer_metrics
├non_trainable_variables
─layers
utrainable_variables
┼metrics
v	variables
 ╞layer_regularization_losses
 

.0

.0
▓
xregularization_losses
╟layer_metrics
╚non_trainable_variables
╔layers
ytrainable_variables
╩metrics
z	variables
 ╦layer_regularization_losses
 
 
 
 
 
 
 
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

И0
Й1

К	variables
 
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUE

Н0
О1

П	variables
 
ca
VARIABLE_VALUEtrue_positives_1=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_negatives>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUE

Т0
У1

Ф	variables
ca
VARIABLE_VALUEtrue_positives_2=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEtrue_negatives=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_1>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_1>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
Ц0
Ч1
Ш2
Щ3

Ъ	variables
ca
VARIABLE_VALUEtrue_positives_3=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_1=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_2>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_2>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
Ь0
Э1
Ю2
Я3

а	variables
ca
VARIABLE_VALUEtrue_positives_4=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_2=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_3>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_3>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
в0
г1
д2
е3

ж	variables
ca
VARIABLE_VALUEtrue_positives_5=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_3=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_4>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_4>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
и0
й1
к2
л3

м	variables
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
╖┤
VARIABLE_VALUETAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/mLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
╢│
VARIABLE_VALUESAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/mLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
╢│
VARIABLE_VALUESAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/mLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
╝╣
VARIABLE_VALUEYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/mLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
╗╕
VARIABLE_VALUEXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/mLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
▒о
VARIABLE_VALUENAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/mLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
пм
VARIABLE_VALUELAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/mLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
╖┤
VARIABLE_VALUETAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/vLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
╢│
VARIABLE_VALUESAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/vLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
╢│
VARIABLE_VALUESAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/vLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
╝╣
VARIABLE_VALUEYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/vLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
╗╕
VARIABLE_VALUEXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/vLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
▒о
VARIABLE_VALUENAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/vLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
пм
VARIABLE_VALUELAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/vLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
z
serving_default_input_1Placeholder*'
_output_shapes
:         )*
dtype0*
shape:         )
z
serving_default_input_2Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_3Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_4Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_5Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
z
serving_default_input_6Placeholder*'
_output_shapes
:         *
dtype0*
shape:         
х
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1serving_default_input_2serving_default_input_3serving_default_input_4serving_default_input_5serving_default_input_6Mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddingsLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddingsLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddingsRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddingsQmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddingsGmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernelEmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8В *.
f)R'
%__inference_signature_wrapper_5960117
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
С
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenameAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOpamsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Read/ReadVariableOp`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Read/ReadVariableOp`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Read/ReadVariableOpfmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Read/ReadVariableOpemsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Read/ReadVariableOp[msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/Read/ReadVariableOpYmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp"true_positives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp$true_positives_1/Read/ReadVariableOp#false_negatives/Read/ReadVariableOp$true_positives_2/Read/ReadVariableOp"true_negatives/Read/ReadVariableOp%false_positives_1/Read/ReadVariableOp%false_negatives_1/Read/ReadVariableOp$true_positives_3/Read/ReadVariableOp$true_negatives_1/Read/ReadVariableOp%false_positives_2/Read/ReadVariableOp%false_negatives_2/Read/ReadVariableOp$true_positives_4/Read/ReadVariableOp$true_negatives_2/Read/ReadVariableOp%false_positives_3/Read/ReadVariableOp%false_negatives_3/Read/ReadVariableOp$true_positives_5/Read/ReadVariableOp$true_negatives_3/Read/ReadVariableOp%false_positives_4/Read/ReadVariableOp%false_negatives_4/Read/ReadVariableOphAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/m/Read/ReadVariableOpgAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/m/Read/ReadVariableOpgAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/m/Read/ReadVariableOpmAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/m/Read/ReadVariableOplAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/m/Read/ReadVariableOpbAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/m/Read/ReadVariableOp`Adam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/m/Read/ReadVariableOphAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/v/Read/ReadVariableOpgAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/v/Read/ReadVariableOpgAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/v/Read/ReadVariableOpmAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/v/Read/ReadVariableOplAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/v/Read/ReadVariableOpbAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/v/Read/ReadVariableOp`Adam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v/Read/ReadVariableOpConst*=
Tin6
422	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *)
f$R"
 __inference__traced_save_5960536
╠
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_rateMmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddingsLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddingsLmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddingsRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddingsQmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddingsGmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernelEmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/biastotalcounttrue_positivesfalse_positivestrue_positives_1false_negativestrue_positives_2true_negativesfalse_positives_1false_negatives_1true_positives_3true_negatives_1false_positives_2false_negatives_2true_positives_4true_negatives_2false_positives_3false_negatives_3true_positives_5true_negatives_3false_positives_4false_negatives_4TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/mSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/mSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/mYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/mXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/mNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/mLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/mTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/vSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/vSAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/vYAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/vXAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/vNAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/vLAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v*<
Tin5
321*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *,
f'R%
#__inference__traced_restore_5960690∙╕	
╡
п
__inference_loss_fn_0_5960290К
xmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_regularizer_square_readvariableop_resource:
identityИвomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpЫ
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpxmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpЁ
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareУ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const╢
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumЗ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x╕
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulЦ
IdentityIdentityamsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul:z:0p^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2т
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp
з
м
*__inference_cat2_vec_layer_call_fn_5960237
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

identityИвStatefulPartitionedCall╩
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3inputs_4unknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin
2
*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         
*'
_read_only_resource_inputs	
	*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_cat2_vec_layer_call_and_return_conditional_losses_59599122
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:         :         :         :         :         : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/4
М╝
Ж
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5959912

inputs
inputs_1
inputs_2
inputs_3
inputs_4Z
Hew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5959849:Y
Gew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5959855:Y
Gew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5959861:_
Mew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5959867:
^
Lew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5959873:

identityИв@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupв?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupвDEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupвEEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupв?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupвomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpвsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpвtmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp╜
4EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/CastCastinputs*

DstT0*

SrcT0*'
_output_shapes
:         26
4EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Castя
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupResourceGatherHew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_59598498EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5959849*+
_output_shapes
:         *
dtype02B
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupо
IEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentityIEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5959849*+
_output_shapes
:         2K
IEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity░
KEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1IdentityREW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2M
KEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1╜
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/CastCastinputs_1*

DstT0*

SrcT0*'
_output_shapes
:         25
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Castъ
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupResourceGatherGew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_59598557EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5959855*+
_output_shapes
:         *
dtype02A
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupк
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentityHEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5959855*+
_output_shapes
:         2J
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identityн
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1IdentityQEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2L
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1╜
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/CastCastinputs_2*

DstT0*

SrcT0*'
_output_shapes
:         25
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Castъ
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupResourceGatherGew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_59598617EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5959861*+
_output_shapes
:         *
dtype02A
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupк
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentityHEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5959861*+
_output_shapes
:         2J
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identityн
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1IdentityQEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2L
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1╔
9EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/CastCastinputs_3*

DstT0*

SrcT0*'
_output_shapes
:         2;
9EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/CastИ
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupResourceGatherMew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5959867=EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*`
_classV
TRloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5959867*+
_output_shapes
:         *
dtype02G
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup┬
NEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentityNEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*`
_classV
TRloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5959867*+
_output_shapes
:         2P
NEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity┐
PEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1IdentityWEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2R
PEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1╟
8EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/CastCastinputs_4*

DstT0*

SrcT0*'
_output_shapes
:         2:
8EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/CastГ
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupResourceGatherLew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5959873<EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*_
_classU
SQloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5959873*+
_output_shapes
:         *
dtype02F
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup╛
MEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentityMEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*_
_classU
SQloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5959873*+
_output_shapes
:         2O
MEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity╝
OEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1IdentityVEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2Q
OEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1t
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╩
concatenate/concatConcatV2TEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0SEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0SEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0YEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0XEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:0 concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:         
2
concatenate/concatМ
SqueezeSqueezeconcatenate/concat:output:0*
T0*'
_output_shapes
:         
*
squeeze_dims

■        2	
Squeezeы
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpHew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5959849*
_output_shapes

:*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpЁ
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareУ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const╢
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumЗ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x╕
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulш
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpGew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5959855*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulш
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpGew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5959861*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul·
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpMew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5959867*
_output_shapes

:
*
dtype02v
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp 
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquare|msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2g
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareЭ
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const╩
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumimsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumС
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x╠
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulmmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulў
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpLew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5959873*
_output_shapes

:
*
dtype02u
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp№
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquare{msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareЫ
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const╞
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumП
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x╚
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMullmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul√
IdentityIdentitySqueeze:output:0A^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup@^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupE^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupF^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup@^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupp^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpt^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpu^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:         :         :         :         :         : : : : : 2Д
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup2В
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup2М
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupDEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup2О
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupEEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup2В
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup2т
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2ъ
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2ь
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOptmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:O K
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs:OK
'
_output_shapes
:         
 
_user_specified_nameinputs
╛▀
│'
#__inference__traced_restore_5960690
file_prefix$
assignvariableop_adam_iter:	 (
assignvariableop_1_adam_beta_1: (
assignvariableop_2_adam_beta_2: '
assignvariableop_3_adam_decay: /
%assignvariableop_4_adam_learning_rate: r
`assignvariableop_5_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings:q
_assignvariableop_6_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings:q
_assignvariableop_7_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings:w
eassignvariableop_8_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings:
v
dassignvariableop_9_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings:
m
[assignvariableop_10_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel:3Mg
Yassignvariableop_11_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias:M#
assignvariableop_12_total: #
assignvariableop_13_count: 0
"assignvariableop_14_true_positives:1
#assignvariableop_15_false_positives:2
$assignvariableop_16_true_positives_1:1
#assignvariableop_17_false_negatives:7
$assignvariableop_18_true_positives_2:	╚M5
"assignvariableop_19_true_negatives:	╚M8
%assignvariableop_20_false_positives_1:	╚M8
%assignvariableop_21_false_negatives_1:	╚M7
$assignvariableop_22_true_positives_3:	╚M7
$assignvariableop_23_true_negatives_1:	╚M8
%assignvariableop_24_false_positives_2:	╚M8
%assignvariableop_25_false_negatives_2:	╚M7
$assignvariableop_26_true_positives_4:	╚M7
$assignvariableop_27_true_negatives_2:	╚M8
%assignvariableop_28_false_positives_3:	╚M8
%assignvariableop_29_false_negatives_3:	╚M7
$assignvariableop_30_true_positives_5:	╚M7
$assignvariableop_31_true_negatives_3:	╚M8
%assignvariableop_32_false_positives_4:	╚M8
%assignvariableop_33_false_negatives_4:	╚Mz
hassignvariableop_34_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_m:y
gassignvariableop_35_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_m:y
gassignvariableop_36_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_m:
massignvariableop_37_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_m:
~
lassignvariableop_38_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_m:
t
bassignvariableop_39_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_m:3Mn
`assignvariableop_40_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_m:Mz
hassignvariableop_41_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_v:y
gassignvariableop_42_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_v:y
gassignvariableop_43_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_v:
massignvariableop_44_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_v:
~
lassignvariableop_45_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_v:
t
bassignvariableop_46_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_v:3Mn
`assignvariableop_47_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_v:M
identity_49ИвAssignVariableOpвAssignVariableOp_1вAssignVariableOp_10вAssignVariableOp_11вAssignVariableOp_12вAssignVariableOp_13вAssignVariableOp_14вAssignVariableOp_15вAssignVariableOp_16вAssignVariableOp_17вAssignVariableOp_18вAssignVariableOp_19вAssignVariableOp_2вAssignVariableOp_20вAssignVariableOp_21вAssignVariableOp_22вAssignVariableOp_23вAssignVariableOp_24вAssignVariableOp_25вAssignVariableOp_26вAssignVariableOp_27вAssignVariableOp_28вAssignVariableOp_29вAssignVariableOp_3вAssignVariableOp_30вAssignVariableOp_31вAssignVariableOp_32вAssignVariableOp_33вAssignVariableOp_34вAssignVariableOp_35вAssignVariableOp_36вAssignVariableOp_37вAssignVariableOp_38вAssignVariableOp_39вAssignVariableOp_4вAssignVariableOp_40вAssignVariableOp_41вAssignVariableOp_42вAssignVariableOp_43вAssignVariableOp_44вAssignVariableOp_45вAssignVariableOp_46вAssignVariableOp_47вAssignVariableOp_5вAssignVariableOp_6вAssignVariableOp_7вAssignVariableOp_8вAssignVariableOp_9Р
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*Ь
valueТBП1B)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_namesЁ
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*u
valuelBj1B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slicesг
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*┌
_output_shapes╟
─:::::::::::::::::::::::::::::::::::::::::::::::::*?
dtypes5
321	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0	*
_output_shapes
:2

IdentityЩ
AssignVariableOpAssignVariableOpassignvariableop_adam_iterIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1г
AssignVariableOp_1AssignVariableOpassignvariableop_1_adam_beta_1Identity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2г
AssignVariableOp_2AssignVariableOpassignvariableop_2_adam_beta_2Identity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3в
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_decayIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4к
AssignVariableOp_4AssignVariableOp%assignvariableop_4_adam_learning_rateIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5х
AssignVariableOp_5AssignVariableOp`assignvariableop_5_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddingsIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6ф
AssignVariableOp_6AssignVariableOp_assignvariableop_6_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddingsIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7ф
AssignVariableOp_7AssignVariableOp_assignvariableop_7_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddingsIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8ъ
AssignVariableOp_8AssignVariableOpeassignvariableop_8_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddingsIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9щ
AssignVariableOp_9AssignVariableOpdassignvariableop_9_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddingsIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10у
AssignVariableOp_10AssignVariableOp[assignvariableop_10_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11с
AssignVariableOp_11AssignVariableOpYassignvariableop_11_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12б
AssignVariableOp_12AssignVariableOpassignvariableop_12_totalIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13б
AssignVariableOp_13AssignVariableOpassignvariableop_13_countIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14к
AssignVariableOp_14AssignVariableOp"assignvariableop_14_true_positivesIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15л
AssignVariableOp_15AssignVariableOp#assignvariableop_15_false_positivesIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16м
AssignVariableOp_16AssignVariableOp$assignvariableop_16_true_positives_1Identity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17л
AssignVariableOp_17AssignVariableOp#assignvariableop_17_false_negativesIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18м
AssignVariableOp_18AssignVariableOp$assignvariableop_18_true_positives_2Identity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19к
AssignVariableOp_19AssignVariableOp"assignvariableop_19_true_negativesIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20н
AssignVariableOp_20AssignVariableOp%assignvariableop_20_false_positives_1Identity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21н
AssignVariableOp_21AssignVariableOp%assignvariableop_21_false_negatives_1Identity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22м
AssignVariableOp_22AssignVariableOp$assignvariableop_22_true_positives_3Identity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23м
AssignVariableOp_23AssignVariableOp$assignvariableop_23_true_negatives_1Identity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24н
AssignVariableOp_24AssignVariableOp%assignvariableop_24_false_positives_2Identity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25н
AssignVariableOp_25AssignVariableOp%assignvariableop_25_false_negatives_2Identity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26м
AssignVariableOp_26AssignVariableOp$assignvariableop_26_true_positives_4Identity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27м
AssignVariableOp_27AssignVariableOp$assignvariableop_27_true_negatives_2Identity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28н
AssignVariableOp_28AssignVariableOp%assignvariableop_28_false_positives_3Identity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29н
AssignVariableOp_29AssignVariableOp%assignvariableop_29_false_negatives_3Identity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30м
AssignVariableOp_30AssignVariableOp$assignvariableop_30_true_positives_5Identity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31м
AssignVariableOp_31AssignVariableOp$assignvariableop_31_true_negatives_3Identity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32н
AssignVariableOp_32AssignVariableOp%assignvariableop_32_false_positives_4Identity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33н
AssignVariableOp_33AssignVariableOp%assignvariableop_33_false_negatives_4Identity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34Ё
AssignVariableOp_34AssignVariableOphassignvariableop_34_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_mIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35я
AssignVariableOp_35AssignVariableOpgassignvariableop_35_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_mIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36я
AssignVariableOp_36AssignVariableOpgassignvariableop_36_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37ї
AssignVariableOp_37AssignVariableOpmassignvariableop_37_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38Ї
AssignVariableOp_38AssignVariableOplassignvariableop_38_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39ъ
AssignVariableOp_39AssignVariableOpbassignvariableop_39_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40ш
AssignVariableOp_40AssignVariableOp`assignvariableop_40_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41Ё
AssignVariableOp_41AssignVariableOphassignvariableop_41_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_vIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42я
AssignVariableOp_42AssignVariableOpgassignvariableop_42_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_vIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43я
AssignVariableOp_43AssignVariableOpgassignvariableop_43_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_vIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44ї
AssignVariableOp_44AssignVariableOpmassignvariableop_44_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_vIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45Ї
AssignVariableOp_45AssignVariableOplassignvariableop_45_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_vIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46ъ
AssignVariableOp_46AssignVariableOpbassignvariableop_46_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_vIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_46n
Identity_47IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:2
Identity_47ш
AssignVariableOp_47AssignVariableOp`assignvariableop_47_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_vIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_479
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp■
Identity_48Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_48ё
Identity_49IdentityIdentity_48:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_49"#
identity_49Identity_49:output:0*u
_input_shapesd
b: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
№
\
@__inference_mlp_layer_call_and_return_conditional_losses_5959937

inputs
identityZ
IdentityIdentityinputs*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:         3:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
▐
r
H__inference_concatenate_layer_call_and_return_conditional_losses_5959931

inputs
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis
concatConcatV2inputsinputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         32
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:         ):         
:O K
'
_output_shapes
:         )
 
_user_specified_nameinputs:OK
'
_output_shapes
:         

 
_user_specified_nameinputs
╕t
┬
 __inference__traced_save_5960536
file_prefix(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableopl
hsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_read_readvariableopk
gsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_read_readvariableopk
gsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_read_readvariableopq
msavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_read_readvariableopp
lsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_read_readvariableopf
bsavev2_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_read_readvariableopd
`savev2_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop-
)savev2_true_positives_read_readvariableop.
*savev2_false_positives_read_readvariableop/
+savev2_true_positives_1_read_readvariableop.
*savev2_false_negatives_read_readvariableop/
+savev2_true_positives_2_read_readvariableop-
)savev2_true_negatives_read_readvariableop0
,savev2_false_positives_1_read_readvariableop0
,savev2_false_negatives_1_read_readvariableop/
+savev2_true_positives_3_read_readvariableop/
+savev2_true_negatives_1_read_readvariableop0
,savev2_false_positives_2_read_readvariableop0
,savev2_false_negatives_2_read_readvariableop/
+savev2_true_positives_4_read_readvariableop/
+savev2_true_negatives_2_read_readvariableop0
,savev2_false_positives_3_read_readvariableop0
,savev2_false_negatives_3_read_readvariableop/
+savev2_true_positives_5_read_readvariableop/
+savev2_true_negatives_3_read_readvariableop0
,savev2_false_positives_4_read_readvariableop0
,savev2_false_negatives_4_read_readvariableops
osavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_m_read_readvariableopr
nsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_m_read_readvariableopr
nsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_m_read_readvariableopx
tsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_m_read_readvariableopw
ssavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_m_read_readvariableopm
isavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_m_read_readvariableopk
gsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_m_read_readvariableops
osavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_v_read_readvariableopr
nsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_v_read_readvariableopr
nsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_v_read_readvariableopx
tsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_v_read_readvariableopw
ssavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_v_read_readvariableopm
isavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_v_read_readvariableopk
gsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_v_read_readvariableop
savev2_const

identity_1ИвMergeV2CheckpointsП
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1Л
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shardж
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilenameК
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*Ь
valueТBП1B)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_namesъ
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*u
valuelBj1B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices 
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableophsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_read_readvariableopgsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_read_readvariableopgsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_read_readvariableopmsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_read_readvariableoplsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_read_readvariableopbsavev2_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_read_readvariableop`savev2_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop)savev2_true_positives_read_readvariableop*savev2_false_positives_read_readvariableop+savev2_true_positives_1_read_readvariableop*savev2_false_negatives_read_readvariableop+savev2_true_positives_2_read_readvariableop)savev2_true_negatives_read_readvariableop,savev2_false_positives_1_read_readvariableop,savev2_false_negatives_1_read_readvariableop+savev2_true_positives_3_read_readvariableop+savev2_true_negatives_1_read_readvariableop,savev2_false_positives_2_read_readvariableop,savev2_false_negatives_2_read_readvariableop+savev2_true_positives_4_read_readvariableop+savev2_true_negatives_2_read_readvariableop,savev2_false_positives_3_read_readvariableop,savev2_false_negatives_3_read_readvariableop+savev2_true_positives_5_read_readvariableop+savev2_true_negatives_3_read_readvariableop,savev2_false_positives_4_read_readvariableop,savev2_false_negatives_4_read_readvariableoposavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_m_read_readvariableopnsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_m_read_readvariableopnsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_m_read_readvariableoptsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_m_read_readvariableopssavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_m_read_readvariableopisavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_m_read_readvariableopgsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_m_read_readvariableoposavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embeddings_v_read_readvariableopnsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_v_read_readvariableopnsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_v_read_readvariableoptsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_v_read_readvariableopssavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_v_read_readvariableopisavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_kernel_v_read_readvariableopgsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *?
dtypes5
321	2
SaveV2║
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixesб
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*╡
_input_shapesг
а: : : : : : ::::
:
:3M:M: : :::::	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M:	╚M::::
:
:3M:M::::
:
:3M:M: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :$ 

_output_shapes

::$ 

_output_shapes

::$ 

_output_shapes

::$	 

_output_shapes

:
:$
 

_output_shapes

:
:$ 

_output_shapes

:3M: 

_output_shapes
:M:

_output_shapes
: :

_output_shapes
: : 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
::%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:%!

_output_shapes
:	╚M:% !

_output_shapes
:	╚M:%!!

_output_shapes
:	╚M:%"!

_output_shapes
:	╚M:$# 

_output_shapes

::$$ 

_output_shapes

::$% 

_output_shapes

::$& 

_output_shapes

:
:$' 

_output_shapes

:
:$( 

_output_shapes

:3M: )

_output_shapes
:M:$* 

_output_shapes

::$+ 

_output_shapes

::$, 

_output_shapes

::$- 

_output_shapes

:
:$. 

_output_shapes

:
:$/ 

_output_shapes

:3M: 0

_output_shapes
:M:1

_output_shapes
: 
н
╣
__inference_loss_fn_3_5960323П
}msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_regularizer_square_readvariableop_resource:

identityИвtmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpк
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOp}msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:
*
dtype02v
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp 
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquare|msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2g
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareЭ
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const╩
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumimsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumС
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x╠
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulmmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulа
IdentityIdentityfmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul:z:0u^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2ь
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOptmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp
Ъ}
А
"__inference__wrapped_model_5959832
input_1
input_2
input_3
input_4
input_5
input_6m
[msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5959790:l
Zmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5959796:l
Zmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5959802:r
`msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5959808:
q
_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5959814:
q
_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource:3Mn
`msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource:M
identityИвSmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupвRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupвWmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupвXmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupвRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupвWmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpвVmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpф
Gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/CastCastinput_2*

DstT0*

SrcT0*'
_output_shapes
:         2I
Gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Cast╬
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupResourceGather[msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5959790Kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*n
_classd
b`loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5959790*+
_output_shapes
:         *
dtype02U
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup·
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentity\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*n
_classd
b`loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5959790*+
_output_shapes
:         2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identityщ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1Identityemsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1т
Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/CastCastinput_3*

DstT0*

SrcT0*'
_output_shapes
:         2H
Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Cast╔
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupResourceGatherZmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5959796Jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*m
_classc
a_loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5959796*+
_output_shapes
:         *
dtype02T
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupЎ
[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentity[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*m
_classc
a_loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5959796*+
_output_shapes
:         2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identityц
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1Identitydmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1т
Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/CastCastinput_4*

DstT0*

SrcT0*'
_output_shapes
:         2H
Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Cast╔
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupResourceGatherZmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5959802Jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*m
_classc
a_loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5959802*+
_output_shapes
:         *
dtype02T
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupЎ
[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentity[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*m
_classc
a_loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5959802*+
_output_shapes
:         2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identityц
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1Identitydmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1ю
Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/CastCastinput_5*

DstT0*

SrcT0*'
_output_shapes
:         2N
Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/Castч
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupResourceGather`msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5959808Pmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*s
_classi
geloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5959808*+
_output_shapes
:         *
dtype02Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupО
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentityamsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*s
_classi
geloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5959808*+
_output_shapes
:         2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity°
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1Identityjmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1ь
Kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/CastCastinput_6*

DstT0*

SrcT0*'
_output_shapes
:         2M
Kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/Castт
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupResourceGather_msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5959814Omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*r
_classh
fdloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5959814*+
_output_shapes
:         *
dtype02Y
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupК
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentity`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*r
_classh
fdloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5959814*+
_output_shapes
:         2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identityї
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1Identityimsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1Ъ
*msd_model/cat2_vec/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2,
*msd_model/cat2_vec/concatenate/concat/axisт
%msd_model/cat2_vec/concatenate/concatConcatV2gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:03msd_model/cat2_vec/concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:         
2'
%msd_model/cat2_vec/concatenate/concat┼
msd_model/cat2_vec/SqueezeSqueeze.msd_model/cat2_vec/concatenate/concat:output:0*
T0*'
_output_shapes
:         
*
squeeze_dims

■        2
msd_model/cat2_vec/SqueezeИ
!msd_model/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2#
!msd_model/concatenate/concat/axis▌
msd_model/concatenate/concatConcatV2input_1#msd_model/cat2_vec/Squeeze:output:0*msd_model/concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:         32
msd_model/concatenate/concat╨
Vmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpReadVariableOp_msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02X
Vmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp╒
Gmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMulMatMul%msd_model/concatenate/concat:output:0^msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M2I
Gmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul╧
Wmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOp`msd_model_decoder_ew_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02Y
Wmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpЕ
Hmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddBiasAddQmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul:product:0_msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M2J
Hmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddд
Hmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidSigmoidQmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd:output:0*
T0*'
_output_shapes
:         M2J
Hmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidИ
IdentityIdentityLmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/Sigmoid:y:0T^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupS^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupX^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupY^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupS^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupX^msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpW^msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*Х
_input_shapesГ
А:         ):         :         :         :         :         : : : : : : : 2к
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupSmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup2и
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup2▓
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupWmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup2┤
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupXmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup2и
Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupRmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup2▓
Wmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpWmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp2░
Vmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpVmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:P L
'
_output_shapes
:         )
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_3:PL
'
_output_shapes
:         
!
_user_specified_name	input_4:PL
'
_output_shapes
:         
!
_user_specified_name	input_5:PL
'
_output_shapes
:         
!
_user_specified_name	input_6
№
\
@__inference_mlp_layer_call_and_return_conditional_losses_5960254

inputs
identityZ
IdentityIdentityinputs*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:         3:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
┤
A
%__inference_mlp_layer_call_fn_5960259

inputs
identity╛
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *I
fDRB
@__inference_mlp_layer_call_and_return_conditional_losses_59599372
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:         3:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
Х
╖
__inference_loss_fn_4_5960334О
|msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_regularizer_square_readvariableop_resource:

identityИвsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpз
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOp|msd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:
*
dtype02u
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp№
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquare{msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareЫ
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const╞
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumП
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x╚
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMullmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulЮ
IdentityIdentityemsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul:z:0t^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2ъ
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp
╔
Y
-__inference_concatenate_layer_call_fn_5960250
inputs_0
inputs_1
identity╙
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *Q
fLRJ
H__inference_concatenate_layer_call_and_return_conditional_losses_59599312
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:         ):         
:Q M
'
_output_shapes
:         )
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         

"
_user_specified_name
inputs/1
з
▒
D__inference_decoder_layer_call_and_return_conditional_losses_5960270

inputs_
Mew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource:3M\
New_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource:M
identityИвEEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpвDEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpЪ
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpReadVariableOpMew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02F
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpА
5EW_avg_embed_triangle_2_richfl2_decoder_output/MatMulMatMulinputsLEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M27
5EW_avg_embed_triangle_2_richfl2_decoder_output/MatMulЩ
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOpNew_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02G
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp╜
6EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddBiasAdd?EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul:product:0MEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M28
6EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddю
6EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidSigmoid?EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd:output:0*
T0*'
_output_shapes
:         M28
6EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidЭ
IdentityIdentity:EW_avg_embed_triangle_2_richfl2_decoder_output/Sigmoid:y:0F^EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpE^EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         3: : 2О
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpEEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp2М
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpDEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
Э
н
__inference_loss_fn_2_5960312Й
wmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_regularizer_square_readvariableop_resource:
identityИвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpШ
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpwmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mulФ
IdentityIdentity`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul:z:0o^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp
з
▒
D__inference_decoder_layer_call_and_return_conditional_losses_5959950

inputs_
Mew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource:3M\
New_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource:M
identityИвEEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpвDEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpЪ
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpReadVariableOpMew_avg_embed_triangle_2_richfl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02F
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpА
5EW_avg_embed_triangle_2_richfl2_decoder_output/MatMulMatMulinputsLEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M27
5EW_avg_embed_triangle_2_richfl2_decoder_output/MatMulЩ
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOpNew_avg_embed_triangle_2_richfl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02G
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp╜
6EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddBiasAdd?EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul:product:0MEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:         M28
6EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAddю
6EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidSigmoid?EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd:output:0*
T0*'
_output_shapes
:         M28
6EW_avg_embed_triangle_2_richfl2_decoder_output/SigmoidЭ
IdentityIdentity:EW_avg_embed_triangle_2_richfl2_decoder_output/Sigmoid:y:0F^EW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpE^EW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         3: : 2О
EEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOpEEW_avg_embed_triangle_2_richfl2_decoder_output/BiasAdd/ReadVariableOp2М
DEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOpDEW_avg_embed_triangle_2_richfl2_decoder_output/MatMul/ReadVariableOp:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
├
х
%__inference_signature_wrapper_5960117
input_1
input_2
input_3
input_4
input_5
input_6
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

	unknown_4:3M
	unknown_5:M
identityИвStatefulPartitionedCall╞
StatefulPartitionedCallStatefulPartitionedCallinput_1input_2input_3input_4input_5input_6unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8В *+
f&R$
"__inference__wrapped_model_59598322
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*Х
_input_shapesГ
А:         ):         :         :         :         :         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         )
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_3:PL
'
_output_shapes
:         
!
_user_specified_name	input_4:PL
'
_output_shapes
:         
!
_user_specified_name	input_5:PL
'
_output_shapes
:         
!
_user_specified_name	input_6
╨~
г
F__inference_msd_model_layer_call_and_return_conditional_losses_5959987
input_1
input_2
input_3
input_4
input_5
input_6"
cat2_vec_5959913:"
cat2_vec_5959915:"
cat2_vec_5959917:"
cat2_vec_5959919:
"
cat2_vec_5959921:
!
decoder_5959951:3M
decoder_5959953:M
identityИв cat2_vec/StatefulPartitionedCallвdecoder/StatefulPartitionedCallвomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpвsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpвtmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp№
 cat2_vec/StatefulPartitionedCallStatefulPartitionedCallinput_2input_3input_4input_5input_6cat2_vec_5959913cat2_vec_5959915cat2_vec_5959917cat2_vec_5959919cat2_vec_5959921*
Tin
2
*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         
*'
_read_only_resource_inputs	
	*-
config_proto

CPU

GPU 2J 8В *N
fIRG
E__inference_cat2_vec_layer_call_and_return_conditional_losses_59599122"
 cat2_vec/StatefulPartitionedCallЛ
concatenate/PartitionedCallPartitionedCallinput_1)cat2_vec/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *Q
fLRJ
H__inference_concatenate_layer_call_and_return_conditional_losses_59599312
concatenate/PartitionedCallф
mlp/PartitionedCallPartitionedCall$concatenate/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8В *I
fDRB
@__inference_mlp_layer_call_and_return_conditional_losses_59599372
mlp/PartitionedCallи
decoder/StatefulPartitionedCallStatefulPartitionedCallmlp/PartitionedCall:output:0decoder_5959951decoder_5959953*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         M*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_decoder_layer_call_and_return_conditional_losses_59599502!
decoder/StatefulPartitionedCall│
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5959913*
_output_shapes

:*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpЁ
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareУ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const╢
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumЗ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x╕
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul▒
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5959915*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul▒
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5959917*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul╜
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5959919*
_output_shapes

:
*
dtype02v
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp 
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquare|msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2g
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareЭ
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const╩
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumimsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumС
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x╠
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulmmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul╗
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5959921*
_output_shapes

:
*
dtype02u
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp№
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquare{msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareЫ
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const╞
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumП
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x╚
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMullmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulВ
IdentityIdentity(decoder/StatefulPartitionedCall:output:0!^cat2_vec/StatefulPartitionedCall ^decoder/StatefulPartitionedCallp^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpt^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpu^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*Х
_input_shapesГ
А:         ):         :         :         :         :         : : : : : : : 2D
 cat2_vec/StatefulPartitionedCall cat2_vec/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2т
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2ъ
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2ь
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOptmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:P L
'
_output_shapes
:         )
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_3:PL
'
_output_shapes
:         
!
_user_specified_name	input_4:PL
'
_output_shapes
:         
!
_user_specified_name	input_5:PL
'
_output_shapes
:         
!
_user_specified_name	input_6
э
ы
+__inference_msd_model_layer_call_fn_5960012
input_1
input_2
input_3
input_4
input_5
input_6
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

	unknown_4:3M
	unknown_5:M
identityИвStatefulPartitionedCallъ
StatefulPartitionedCallStatefulPartitionedCallinput_1input_2input_3input_4input_5input_6unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8В *O
fJRH
F__inference_msd_model_layer_call_and_return_conditional_losses_59599872
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*Х
_input_shapesГ
А:         ):         :         :         :         :         : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:         )
!
_user_specified_name	input_1:PL
'
_output_shapes
:         
!
_user_specified_name	input_2:PL
'
_output_shapes
:         
!
_user_specified_name	input_3:PL
'
_output_shapes
:         
!
_user_specified_name	input_4:PL
'
_output_shapes
:         
!
_user_specified_name	input_5:PL
'
_output_shapes
:         
!
_user_specified_name	input_6
ч
t
H__inference_concatenate_layer_call_and_return_conditional_losses_5960244
inputs_0
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axisБ
concatConcatV2inputs_0inputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:         32
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:         32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:         ):         
:Q M
'
_output_shapes
:         )
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         

"
_user_specified_name
inputs/1
Ъ╝
И
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5960218
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4Z
Hew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5960155:Y
Gew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5960161:Y
Gew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5960167:_
Mew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5960173:
^
Lew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5960179:

identityИв@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupв?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupвDEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupвEEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupв?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupвomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpвsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpвtmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp┐
4EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/CastCastinputs_0*

DstT0*

SrcT0*'
_output_shapes
:         26
4EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Castя
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupResourceGatherHew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_59601558EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5960155*+
_output_shapes
:         *
dtype02B
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookupо
IEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentityIEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/5960155*+
_output_shapes
:         2K
IEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity░
KEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1IdentityREW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2M
KEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1╜
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/CastCastinputs_1*

DstT0*

SrcT0*'
_output_shapes
:         25
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Castъ
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupResourceGatherGew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_59601617EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5960161*+
_output_shapes
:         *
dtype02A
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupк
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentityHEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/5960161*+
_output_shapes
:         2J
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identityн
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1IdentityQEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2L
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1╜
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/CastCastinputs_2*

DstT0*

SrcT0*'
_output_shapes
:         25
3EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Castъ
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupResourceGatherGew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_59601677EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5960167*+
_output_shapes
:         *
dtype02A
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupк
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentityHEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*Z
_classP
NLloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/5960167*+
_output_shapes
:         2J
HEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identityн
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1IdentityQEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2L
JEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1╔
9EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/CastCastinputs_3*

DstT0*

SrcT0*'
_output_shapes
:         2;
9EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/CastИ
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupResourceGatherMew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5960173=EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*`
_classV
TRloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5960173*+
_output_shapes
:         *
dtype02G
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup┬
NEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentityNEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*`
_classV
TRloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/5960173*+
_output_shapes
:         2P
NEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity┐
PEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1IdentityWEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2R
PEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1╟
8EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/CastCastinputs_4*

DstT0*

SrcT0*'
_output_shapes
:         2:
8EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/CastГ
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupResourceGatherLew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5960179<EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*_
_classU
SQloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5960179*+
_output_shapes
:         *
dtype02F
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup╛
MEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentityMEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*_
_classU
SQloc:@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/5960179*+
_output_shapes
:         2O
MEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity╝
OEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1IdentityVEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:         2Q
OEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1t
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis╩
concatenate/concatConcatV2TEW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0SEW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0SEW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0YEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0XEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:0 concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:         
2
concatenate/concatМ
SqueezeSqueezeconcatenate/concat:output:0*
T0*'
_output_shapes
:         
*
squeeze_dims

■        2	
Squeezeы
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpHew_avg_embed_triangle_2_richfl2_cat2vec_emb_clc_embedding_lookup_5960155*
_output_shapes

:*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpЁ
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SquareУ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const╢
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/SumЗ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x╕
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/mulш
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpGew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embedding_lookup_5960161*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulш
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpGew_avg_embed_triangle_2_richfl2_cat2vec_emb_hg_embedding_lookup_5960167*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/mul·
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpMew_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1_embedding_lookup_5960173*
_output_shapes

:
*
dtype02v
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp 
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquare|msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2g
emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareЭ
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const╩
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumimsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumС
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x╠
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulmmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0kmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2d
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulў
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpLew_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top_embedding_lookup_5960179*
_output_shapes

:
*
dtype02u
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp№
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquare{msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2f
dmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareЫ
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const╞
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumhmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/SumП
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2e
cmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x╚
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMullmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0jmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/mul√
IdentityIdentitySqueeze:output:0A^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup@^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookupE^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupF^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup@^EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookupp^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpt^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpu^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpo^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:         
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:         :         :         :         :         : : : : : 2Д
@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup@EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embedding_lookup2В
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embedding_lookup2М
DEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookupDEW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embedding_lookup2О
EEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookupEEW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embedding_lookup2В
?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup?EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embedding_lookup2т
omsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2ъ
smsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpsmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2ь
tmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOptmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:Q M
'
_output_shapes
:         
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:         
"
_user_specified_name
inputs/4
Ы
Ц
)__inference_decoder_layer_call_fn_5960279

inputs
unknown:3M
	unknown_0:M
identityИвStatefulPartitionedCallЇ
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:         M*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8В *M
fHRF
D__inference_decoder_layer_call_and_return_conditional_losses_59599502
StatefulPartitionedCallО
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:         M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:         3: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:         3
 
_user_specified_nameinputs
Э
н
__inference_loss_fn_1_5960301Й
wmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_regularizer_square_readvariableop_resource:
identityИвnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpШ
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpwmsd_model_cat2_vec_ew_avg_embed_triangle_2_richfl2_cat2vec_emb_wr_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02p
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpэ
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarevmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SquareС
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const▓
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumSumcmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/SumЕ
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
╫#<2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x┤
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulgmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0emsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mulФ
IdentityIdentity`msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/mul:z:0o^msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2р
nmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpnmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp"╠L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*▄
serving_default╚
;
input_10
serving_default_input_1:0         )
;
input_20
serving_default_input_2:0         
;
input_30
serving_default_input_3:0         
;
input_40
serving_default_input_4:0         
;
input_50
serving_default_input_5:0         
;
input_60
serving_default_input_6:0         <
output_10
StatefulPartitionedCall:0         Mtensorflow/serving/predict:Ф╫
Я░
numvars
catvars
	archi
cat2vec
fconcat
fe
response
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
+┌&call_and_return_all_conditional_losses
█_default_save_signature
▄__call__"√н
_tf_keras_modelрн{"name": "msd_model", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "MSDModel", "config": {"layer was saved without config": true}, "is_graph_network": false, "save_spec": {"class_name": "__tuple__", "items": [{"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 41]}, "float32", "input_1"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_2"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_3"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_4"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_5"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_6"]}]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "MSDModel"}, "training_config": {"loss": "rich_loss", "metrics": [[{"class_name": "Precision", "config": {"name": "precision", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 0}, {"class_name": "Recall", "config": {"name": "recall", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 1}, {"class_name": "AUC", "config": {"name": "rocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 2}, {"class_name": "AUC", "config": {"name": "prauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 3}, {"class_name": "AUC", "config": {"name": "wrocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 4}, {"class_name": "AUC", "config": {"name": "wprauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 5}]], "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0010000000474974513, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
 "
trackable_list_wrapper
 "
trackable_list_wrapper
3
	embed
fe"
trackable_dict_wrapper
┴

config
embed_layers
regularization_losses
trainable_variables
	variables
	keras_api
+▌&call_and_return_all_conditional_losses
▐__call__"Т
_tf_keras_layer°{"name": "cat2_vec", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Cat2Vec", "config": {"layer was saved without config": true}}
т
regularization_losses
trainable_variables
	variables
	keras_api
+▀&call_and_return_all_conditional_losses
р__call__"╤
_tf_keras_layer╖{"name": "concatenate", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "shared_object_id": 6, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 41]}, {"class_name": "TensorShape", "items": [null, 10]}]}
▓

config

layers
regularization_losses
trainable_variables
	variables
	keras_api
+с&call_and_return_all_conditional_losses
т__call__"Й
_tf_keras_layerя{"name": "mlp", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "MLP", "config": {"layer was saved without config": true}}
╞

config
fe
 
prediction
!regularization_losses
"trainable_variables
#	variables
$	keras_api
+у&call_and_return_all_conditional_losses
ф__call__"С
_tf_keras_layerў{"name": "decoder", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Decoder", "config": {"layer was saved without config": true}}
▀
%iter

&beta_1

'beta_2
	(decay
)learning_rate*m╠+m═,m╬-m╧.m╨/m╤0m╥*v╙+v╘,v╒-v╓.v╫/v╪0v┘"
	optimizer
 "
trackable_list_wrapper
Q
*0
+1
,2
-3
.4
/5
06"
trackable_list_wrapper
Q
*0
+1
,2
-3
.4
/5
06"
trackable_list_wrapper
╬
	regularization_losses
1layer_metrics
2non_trainable_variables

3layers

trainable_variables
4metrics
	variables
5layer_regularization_losses
▄__call__
█_default_save_signature
+┌&call_and_return_all_conditional_losses
'┌"call_and_return_conditional_losses"
_generic_user_object
-
хserving_default"
signature_map
.
6features"
trackable_dict_wrapper
;
	7archi
8
activation"
trackable_dict_wrapper
C
90
:1
;2
<3
=4"
trackable_list_wrapper
H
ц0
ч1
ш2
щ3
ъ4"
trackable_list_wrapper
C
*0
+1
,2
-3
.4"
trackable_list_wrapper
C
*0
+1
,2
-3
.4"
trackable_list_wrapper
░
regularization_losses
>layer_metrics
?non_trainable_variables

@layers
trainable_variables
Ametrics
	variables
Blayer_regularization_losses
▐__call__
+▌&call_and_return_all_conditional_losses
'▌"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
░
regularization_losses
Clayer_metrics
Dnon_trainable_variables

Elayers
trainable_variables
Fmetrics
	variables
Glayer_regularization_losses
р__call__
+▀&call_and_return_all_conditional_losses
'▀"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
░
regularization_losses
Hlayer_metrics
Inon_trainable_variables

Jlayers
trainable_variables
Kmetrics
	variables
Llayer_regularization_losses
т__call__
+с&call_and_return_all_conditional_losses
'с"call_and_return_conditional_losses"
_generic_user_object
)
Mmlp"
trackable_dict_wrapper
┤

Mconfig

Nlayers
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
+ы&call_and_return_all_conditional_losses
ь__call__"Л
_tf_keras_layerё{"name": "mlp_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "MLP", "config": {"layer was saved without config": true}}
ё

/kernel
0bias
Sregularization_losses
Ttrainable_variables
U	variables
V	keras_api
+э&call_and_return_all_conditional_losses
ю__call__"╩
_tf_keras_layer░{"name": "EW_avg_embed_triangle_2_richfl2_decoder_output", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Dense", "config": {"name": "EW_avg_embed_triangle_2_richfl2_decoder_output", "trainable": true, "dtype": "float32", "units": 77, "activation": "sigmoid", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 7}, "bias_initializer": "my_bias_init", "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 8, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 51}}, "shared_object_id": 9}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 51]}}
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
░
!regularization_losses
Wlayer_metrics
Xnon_trainable_variables

Ylayers
"trainable_variables
Zmetrics
#	variables
[layer_regularization_losses
ф__call__
+у&call_and_return_all_conditional_losses
'у"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
_:]2Mmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings
^:\2Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings
^:\2Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings
d:b
2Rmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings
c:a
2Qmsd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings
Y:W3M2Gmsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel
S:QM2Emsd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
Q
\0
]1
^2
_3
`4
a5
b6"
trackable_list_wrapper
 "
trackable_list_wrapper
C
c0
d1
e2
f3
g4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
Ї
*
embeddings
hregularization_losses
itrainable_variables
j	variables
k	keras_api
+я&call_and_return_all_conditional_losses
Ё__call__"╙
_tf_keras_layer╣{"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 16, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 10}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 11}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 12, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
ё
+
embeddings
lregularization_losses
mtrainable_variables
n	variables
o	keras_api
+ё&call_and_return_all_conditional_losses
Є__call__"╨
_tf_keras_layer╢{"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 5, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 13}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 14}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 15, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
Є
,
embeddings
pregularization_losses
qtrainable_variables
r	variables
s	keras_api
+є&call_and_return_all_conditional_losses
Ї__call__"╤
_tf_keras_layer╖{"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 11, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 16}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 17}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 18, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
■
-
embeddings
tregularization_losses
utrainable_variables
v	variables
w	keras_api
+ї&call_and_return_all_conditional_losses
Ў__call__"▌
_tf_keras_layer├{"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 10, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 19}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 20}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 21, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
№
.
embeddings
xregularization_losses
ytrainable_variables
z	variables
{	keras_api
+ў&call_and_return_all_conditional_losses
°__call__"█
_tf_keras_layer┴{"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 10, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 22}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 23}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 24, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
C
90
:1
;2
<3
=4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
;
	|archi
}
activation"
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
│
Oregularization_losses
~layer_metrics
non_trainable_variables
Аlayers
Ptrainable_variables
Бmetrics
Q	variables
 Вlayer_regularization_losses
ь__call__
+ы&call_and_return_all_conditional_losses
'ы"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
╡
Sregularization_losses
Гlayer_metrics
Дnon_trainable_variables
Еlayers
Ttrainable_variables
Жmetrics
U	variables
 Зlayer_regularization_losses
ю__call__
+э&call_and_return_all_conditional_losses
'э"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
╪

Иtotal

Йcount
К	variables
Л	keras_api"Э
_tf_keras_metricВ{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}, "shared_object_id": 25}
╛
М
thresholds
Нtrue_positives
Оfalse_positives
П	variables
Р	keras_api"▀
_tf_keras_metric─{"class_name": "Precision", "name": "precision", "dtype": "float32", "config": {"name": "precision", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 0}
╡
С
thresholds
Тtrue_positives
Уfalse_negatives
Ф	variables
Х	keras_api"╓
_tf_keras_metric╗{"class_name": "Recall", "name": "recall", "dtype": "float32", "config": {"name": "recall", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 1}
Ъ#
Цtrue_positives
Чtrue_negatives
Шfalse_positives
Щfalse_negatives
Ъ	variables
Ы	keras_api"б"
_tf_keras_metricЖ"{"class_name": "AUC", "name": "rocauc", "dtype": "float32", "config": {"name": "rocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 2, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
Ч#
Ьtrue_positives
Эtrue_negatives
Юfalse_positives
Яfalse_negatives
а	variables
б	keras_api"Ю"
_tf_keras_metricГ"{"class_name": "AUC", "name": "prauc", "dtype": "float32", "config": {"name": "prauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 3, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
Щ0
вtrue_positives
гtrue_negatives
дfalse_positives
еfalse_negatives
ж	variables
з	keras_api"а/
_tf_keras_metricЕ/{"class_name": "AUC", "name": "wrocauc", "dtype": "float32", "config": {"name": "wrocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 4, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
Ц0
иtrue_positives
йtrue_negatives
кfalse_positives
лfalse_negatives
м	variables
н	keras_api"Э/
_tf_keras_metricВ/{"class_name": "AUC", "name": "wprauc", "dtype": "float32", "config": {"name": "wprauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 5, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
(
о3"
trackable_list_wrapper
(
п3"
trackable_list_wrapper
(
░3"
trackable_list_wrapper
(
▒3"
trackable_list_wrapper
(
▓3"
trackable_list_wrapper
(
ц0"
trackable_list_wrapper
'
*0"
trackable_list_wrapper
'
*0"
trackable_list_wrapper
╡
hregularization_losses
│layer_metrics
┤non_trainable_variables
╡layers
itrainable_variables
╢metrics
j	variables
 ╖layer_regularization_losses
Ё__call__
+я&call_and_return_all_conditional_losses
'я"call_and_return_conditional_losses"
_generic_user_object
(
ч0"
trackable_list_wrapper
'
+0"
trackable_list_wrapper
'
+0"
trackable_list_wrapper
╡
lregularization_losses
╕layer_metrics
╣non_trainable_variables
║layers
mtrainable_variables
╗metrics
n	variables
 ╝layer_regularization_losses
Є__call__
+ё&call_and_return_all_conditional_losses
'ё"call_and_return_conditional_losses"
_generic_user_object
(
ш0"
trackable_list_wrapper
'
,0"
trackable_list_wrapper
'
,0"
trackable_list_wrapper
╡
pregularization_losses
╜layer_metrics
╛non_trainable_variables
┐layers
qtrainable_variables
└metrics
r	variables
 ┴layer_regularization_losses
Ї__call__
+є&call_and_return_all_conditional_losses
'є"call_and_return_conditional_losses"
_generic_user_object
(
щ0"
trackable_list_wrapper
'
-0"
trackable_list_wrapper
'
-0"
trackable_list_wrapper
╡
tregularization_losses
┬layer_metrics
├non_trainable_variables
─layers
utrainable_variables
┼metrics
v	variables
 ╞layer_regularization_losses
Ў__call__
+ї&call_and_return_all_conditional_losses
'ї"call_and_return_conditional_losses"
_generic_user_object
(
ъ0"
trackable_list_wrapper
'
.0"
trackable_list_wrapper
'
.0"
trackable_list_wrapper
╡
xregularization_losses
╟layer_metrics
╚non_trainable_variables
╔layers
ytrainable_variables
╩metrics
z	variables
 ╦layer_regularization_losses
°__call__
+ў&call_and_return_all_conditional_losses
'ў"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
И0
Й1"
trackable_list_wrapper
.
К	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_positives
0
Н0
О1"
trackable_list_wrapper
.
П	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_negatives
0
Т0
У1"
trackable_list_wrapper
.
Ф	variables"
_generic_user_object
#:!	╚M (2true_positives
#:!	╚M (2true_negatives
$:"	╚M (2false_positives
$:"	╚M (2false_negatives
@
Ц0
Ч1
Ш2
Щ3"
trackable_list_wrapper
.
Ъ	variables"
_generic_user_object
#:!	╚M (2true_positives
#:!	╚M (2true_negatives
$:"	╚M (2false_positives
$:"	╚M (2false_negatives
@
Ь0
Э1
Ю2
Я3"
trackable_list_wrapper
.
а	variables"
_generic_user_object
#:!	╚M (2true_positives
#:!	╚M (2true_negatives
$:"	╚M (2false_positives
$:"	╚M (2false_negatives
@
в0
г1
д2
е3"
trackable_list_wrapper
.
ж	variables"
_generic_user_object
#:!	╚M (2true_positives
#:!	╚M (2true_negatives
$:"	╚M (2false_positives
$:"	╚M (2false_negatives
@
и0
й1
к2
л3"
trackable_list_wrapper
.
м	variables"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
ц0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
ч0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
ш0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
щ0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
ъ0"
trackable_list_wrapper
d:b2TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/m
c:a2SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/m
c:a2SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/m
i:g
2YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/m
h:f
2XAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/m
^:\3M2NAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/m
X:VM2LAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/m
d:b2TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_clc/embeddings/v
c:a2SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_wr/embeddings/v
c:a2SAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_hg/embeddings/v
i:g
2YAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_parmado1/embeddings/v
h:f
2XAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_richfl2_cat2vec_emb_min_top/embeddings/v
^:\3M2NAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/kernel/v
X:VM2LAdam/msd_model/decoder/EW_avg_embed_triangle_2_richfl2_decoder_output/bias/v
╠2╔
F__inference_msd_model_layer_call_and_return_conditional_losses_5959987■
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
Ш2Х
"__inference__wrapped_model_5959832ю
Л▓З
FullArgSpec
argsЪ 
varargsjargs
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
▒2о
+__inference_msd_model_layer_call_fn_5960012■
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
я2ь
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5960218в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╘2╤
*__inference_cat2_vec_layer_call_fn_5960237в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
Є2я
H__inference_concatenate_layer_call_and_return_conditional_losses_5960244в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╫2╘
-__inference_concatenate_layer_call_fn_5960250в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ъ2ч
@__inference_mlp_layer_call_and_return_conditional_losses_5960254в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╧2╠
%__inference_mlp_layer_call_fn_5960259в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ю2ы
D__inference_decoder_layer_call_and_return_conditional_losses_5960270в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
╙2╨
)__inference_decoder_layer_call_fn_5960279в
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
ўBЇ
%__inference_signature_wrapper_5960117input_1input_2input_3input_4input_5input_6"Ф
Н▓Й
FullArgSpec
argsЪ 
varargs
 
varkwjkwargs
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
┤2▒
__inference_loss_fn_0_5960290П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
┤2▒
__inference_loss_fn_1_5960301П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
┤2▒
__inference_loss_fn_2_5960312П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
┤2▒
__inference_loss_fn_3_5960323П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
┤2▒
__inference_loss_fn_4_5960334П
З▓Г
FullArgSpec
argsЪ 
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *в 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 
и2ев
Щ▓Х
FullArgSpec
argsЪ
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargsЪ 
kwonlydefaults
 
annotationsк *
 ╤
"__inference__wrapped_model_5959832к*+,-./0щвх
▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
к "3к0
.
output_1"К
output_1         M╞
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5960218№*+,-.╦в╟
┐в╗
╕в┤
"К
inputs/0         
"К
inputs/1         
"К
inputs/2         
"К
inputs/3         
"К
inputs/4         
к "%в"
К
0         

Ъ Ю
*__inference_cat2_vec_layer_call_fn_5960237я*+,-.╦в╟
┐в╗
╕в┤
"К
inputs/0         
"К
inputs/1         
"К
inputs/2         
"К
inputs/3         
"К
inputs/4         
к "К         
╨
H__inference_concatenate_layer_call_and_return_conditional_losses_5960244ГZвW
PвM
KЪH
"К
inputs/0         )
"К
inputs/1         

к "%в"
К
0         3
Ъ з
-__inference_concatenate_layer_call_fn_5960250vZвW
PвM
KЪH
"К
inputs/0         )
"К
inputs/1         

к "К         3д
D__inference_decoder_layer_call_and_return_conditional_losses_5960270\/0/в,
%в"
 К
inputs         3
к "%в"
К
0         M
Ъ |
)__inference_decoder_layer_call_fn_5960279O/0/в,
%в"
 К
inputs         3
к "К         M<
__inference_loss_fn_0_5960290*в

в 
к "К <
__inference_loss_fn_1_5960301+в

в 
к "К <
__inference_loss_fn_2_5960312,в

в 
к "К <
__inference_loss_fn_3_5960323-в

в 
к "К <
__inference_loss_fn_4_5960334.в

в 
к "К Ь
@__inference_mlp_layer_call_and_return_conditional_losses_5960254X/в,
%в"
 К
inputs         3
к "%в"
К
0         3
Ъ t
%__inference_mlp_layer_call_fn_5960259K/в,
%в"
 К
inputs         3
к "К         3ч
F__inference_msd_model_layer_call_and_return_conditional_losses_5959987Ь*+,-./0щвх
▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
к "%в"
К
0         M
Ъ ┐
+__inference_msd_model_layer_call_fn_5960012П*+,-./0щвх
▌в┘
╓в╥
!К
input_1         )
!К
input_2         
!К
input_3         
!К
input_4         
!К
input_5         
!К
input_6         
к "К         MП
%__inference_signature_wrapper_5960117х*+,-./0два
в 
ШкФ
,
input_1!К
input_1         )
,
input_2!К
input_2         
,
input_3!К
input_3         
,
input_4!К
input_4         
,
input_5!К
input_5         
,
input_6!К
input_6         "3к0
.
output_1"К
output_1         M