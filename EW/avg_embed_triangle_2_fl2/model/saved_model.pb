�
��
.
Abs
x"T
y"T"
Ttype:

2	
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
*
Erf
x"T
y"T"
Ttype:
2
+
Erfc
x"T
y"T"
Ttype:
2
=
Greater
x"T
y"T
z
"
Ttype:
2	
.
Identity

input"T
output"T"	
Ttype
:
Less
x"T
y"T
z
"
Ttype:
2	
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
@
RealDiv
x"T
y"T
z"T"
Ttype:
2	
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
A
SelectV2
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
Sub
x"T
y"T
z"T"
Ttype:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.5.02v2.5.0-rc3-213-ga4dfb8d1a718��
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
�
Imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*Z
shared_nameKImsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings
�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Read/ReadVariableOpReadVariableOpImsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings*
_output_shapes

:*
dtype0
�
Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*Y
shared_nameJHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings
�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Read/ReadVariableOpReadVariableOpHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings*
_output_shapes

:*
dtype0
�
Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*Y
shared_nameJHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings
�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Read/ReadVariableOpReadVariableOpHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings*
_output_shapes

:*
dtype0
�
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*_
shared_namePNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings
�
bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Read/ReadVariableOpReadVariableOpNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings*
_output_shapes

:
*
dtype0
�
Mmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*^
shared_nameOMmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings
�
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Read/ReadVariableOpReadVariableOpMmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings*
_output_shapes

:
*
dtype0
�
Cmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*T
shared_nameECmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel
�
Wmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/Read/ReadVariableOpReadVariableOpCmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel*
_output_shapes

:3M*
dtype0
�
Amsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*R
shared_nameCAmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias
�
Umsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/Read/ReadVariableOpReadVariableOpAmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias*
_output_shapes
:M*
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
t
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametrue_positives
m
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes
:*
dtype0
v
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_positives
o
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes
:*
dtype0
x
true_positives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nametrue_positives_1
q
$true_positives_1/Read/ReadVariableOpReadVariableOptrue_positives_1*
_output_shapes
:*
dtype0
v
false_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_negatives
o
#false_negatives/Read/ReadVariableOpReadVariableOpfalse_negatives*
_output_shapes
:*
dtype0
}
true_positives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_positives_2
v
$true_positives_2/Read/ReadVariableOpReadVariableOptrue_positives_2*
_output_shapes
:	�M*
dtype0
y
true_negativesVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*
shared_nametrue_negatives
r
"true_negatives/Read/ReadVariableOpReadVariableOptrue_negatives*
_output_shapes
:	�M*
dtype0

false_positives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_positives_1
x
%false_positives_1/Read/ReadVariableOpReadVariableOpfalse_positives_1*
_output_shapes
:	�M*
dtype0

false_negatives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_negatives_1
x
%false_negatives_1/Read/ReadVariableOpReadVariableOpfalse_negatives_1*
_output_shapes
:	�M*
dtype0
}
true_positives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_positives_3
v
$true_positives_3/Read/ReadVariableOpReadVariableOptrue_positives_3*
_output_shapes
:	�M*
dtype0
}
true_negatives_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_negatives_1
v
$true_negatives_1/Read/ReadVariableOpReadVariableOptrue_negatives_1*
_output_shapes
:	�M*
dtype0

false_positives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_positives_2
x
%false_positives_2/Read/ReadVariableOpReadVariableOpfalse_positives_2*
_output_shapes
:	�M*
dtype0

false_negatives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_negatives_2
x
%false_negatives_2/Read/ReadVariableOpReadVariableOpfalse_negatives_2*
_output_shapes
:	�M*
dtype0
}
true_positives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_positives_4
v
$true_positives_4/Read/ReadVariableOpReadVariableOptrue_positives_4*
_output_shapes
:	�M*
dtype0
}
true_negatives_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_negatives_2
v
$true_negatives_2/Read/ReadVariableOpReadVariableOptrue_negatives_2*
_output_shapes
:	�M*
dtype0

false_positives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_positives_3
x
%false_positives_3/Read/ReadVariableOpReadVariableOpfalse_positives_3*
_output_shapes
:	�M*
dtype0

false_negatives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_negatives_3
x
%false_negatives_3/Read/ReadVariableOpReadVariableOpfalse_negatives_3*
_output_shapes
:	�M*
dtype0
}
true_positives_5VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_positives_5
v
$true_positives_5/Read/ReadVariableOpReadVariableOptrue_positives_5*
_output_shapes
:	�M*
dtype0
}
true_negatives_3VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*!
shared_nametrue_negatives_3
v
$true_negatives_3/Read/ReadVariableOpReadVariableOptrue_negatives_3*
_output_shapes
:	�M*
dtype0

false_positives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_positives_4
x
%false_positives_4/Read/ReadVariableOpReadVariableOpfalse_positives_4*
_output_shapes
:	�M*
dtype0

false_negatives_4VarHandleOp*
_output_shapes
: *
dtype0*
shape:	�M*"
shared_namefalse_negatives_4
x
%false_negatives_4/Read/ReadVariableOpReadVariableOpfalse_negatives_4*
_output_shapes
:	�M*
dtype0
�
PAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*a
shared_nameRPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/m
�
dAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/m/Read/ReadVariableOpReadVariableOpPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/m*
_output_shapes

:*
dtype0
�
OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*`
shared_nameQOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/m
�
cAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/m/Read/ReadVariableOpReadVariableOpOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/m*
_output_shapes

:*
dtype0
�
OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*`
shared_nameQOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/m
�
cAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/m/Read/ReadVariableOpReadVariableOpOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/m*
_output_shapes

:*
dtype0
�
UAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*f
shared_nameWUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/m
�
iAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/m/Read/ReadVariableOpReadVariableOpUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/m*
_output_shapes

:
*
dtype0
�
TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*e
shared_nameVTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/m
�
hAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/m/Read/ReadVariableOpReadVariableOpTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/m*
_output_shapes

:
*
dtype0
�
JAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*[
shared_nameLJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/m
�
^Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/m/Read/ReadVariableOpReadVariableOpJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/m*
_output_shapes

:3M*
dtype0
�
HAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*Y
shared_nameJHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/m
�
\Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/m/Read/ReadVariableOpReadVariableOpHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/m*
_output_shapes
:M*
dtype0
�
PAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*a
shared_nameRPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/v
�
dAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/v/Read/ReadVariableOpReadVariableOpPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/v*
_output_shapes

:*
dtype0
�
OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*`
shared_nameQOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/v
�
cAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/v/Read/ReadVariableOpReadVariableOpOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/v*
_output_shapes

:*
dtype0
�
OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*`
shared_nameQOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/v
�
cAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/v/Read/ReadVariableOpReadVariableOpOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/v*
_output_shapes

:*
dtype0
�
UAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*f
shared_nameWUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/v
�
iAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/v/Read/ReadVariableOpReadVariableOpUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/v*
_output_shapes

:
*
dtype0
�
TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*e
shared_nameVTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/v
�
hAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/v/Read/ReadVariableOpReadVariableOpTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/v*
_output_shapes

:
*
dtype0
�
JAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:3M*[
shared_nameLJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/v
�
^Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/v/Read/ReadVariableOpReadVariableOpJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/v*
_output_shapes

:3M*
dtype0
�
HAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:M*Y
shared_nameJHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v
�
\Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v/Read/ReadVariableOpReadVariableOpHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v*
_output_shapes
:M*
dtype0

NoOpNoOp
�\
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�\
value�[B�[ B�[
�
numvars
catvars
	archi
cat2vec
fconcat
fe
response
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
 
 

	embed
fe
p

config
embed_layers
regularization_losses
trainable_variables
	variables
	keras_api
R
regularization_losses
trainable_variables
	variables
	keras_api
j

config

layers
regularization_losses
trainable_variables
	variables
	keras_api
v

config
fe
 
prediction
!regularization_losses
"trainable_variables
#	variables
$	keras_api
�
%iter

&beta_1

'beta_2
	(decay
)learning_rate*m�+m�,m�-m�.m�/m�0m�*v�+v�,v�-v�.v�/v�0v�
 
1
*0
+1
,2
-3
.4
/5
06
1
*0
+1
,2
-3
.4
/5
06
�
	regularization_losses
1layer_metrics
2non_trainable_variables

3layers

trainable_variables
4metrics
	variables
5layer_regularization_losses
 

6features

	7archi
8
activation
#
90
:1
;2
<3
=4
 
#
*0
+1
,2
-3
.4
#
*0
+1
,2
-3
.4
�
regularization_losses
>layer_metrics
?non_trainable_variables

@layers
trainable_variables
Ametrics
	variables
Blayer_regularization_losses
 
 
 
�
regularization_losses
Clayer_metrics
Dnon_trainable_variables

Elayers
trainable_variables
Fmetrics
	variables
Glayer_regularization_losses
 
 
 
 
�
regularization_losses
Hlayer_metrics
Inon_trainable_variables

Jlayers
trainable_variables
Kmetrics
	variables
Llayer_regularization_losses
	
Mmlp
j

Mconfig

Nlayers
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
h

/kernel
0bias
Sregularization_losses
Ttrainable_variables
U	variables
V	keras_api
 

/0
01

/0
01
�
!regularization_losses
Wlayer_metrics
Xnon_trainable_variables

Ylayers
"trainable_variables
Zmetrics
#	variables
[layer_regularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEImsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUENmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEMmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUECmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUE
 
 

0
1
2
3
1
\0
]1
^2
_3
`4
a5
b6
 
#
c0
d1
e2
f3
g4
 
 
b
*
embeddings
hregularization_losses
itrainable_variables
j	variables
k	keras_api
b
+
embeddings
lregularization_losses
mtrainable_variables
n	variables
o	keras_api
b
,
embeddings
pregularization_losses
qtrainable_variables
r	variables
s	keras_api
b
-
embeddings
tregularization_losses
utrainable_variables
v	variables
w	keras_api
b
.
embeddings
xregularization_losses
ytrainable_variables
z	variables
{	keras_api
 
 
#
90
:1
;2
<3
=4
 
 
 
 
 
 
 
 
 
 
 
 

	|archi
}
activation
 
 
 
 
�
Oregularization_losses
~layer_metrics
non_trainable_variables
�layers
Ptrainable_variables
�metrics
Q	variables
 �layer_regularization_losses
 

/0
01

/0
01
�
Sregularization_losses
�layer_metrics
�non_trainable_variables
�layers
Ttrainable_variables
�metrics
U	variables
 �layer_regularization_losses
 
 

0
 1
 
 
8

�total

�count
�	variables
�	keras_api
\
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api
\
�
thresholds
�true_positives
�false_negatives
�	variables
�	keras_api
v
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api
v
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api
v
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api
v
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api

�3

�3

�3

�3

�3
 

*0

*0
�
hregularization_losses
�layer_metrics
�non_trainable_variables
�layers
itrainable_variables
�metrics
j	variables
 �layer_regularization_losses
 

+0

+0
�
lregularization_losses
�layer_metrics
�non_trainable_variables
�layers
mtrainable_variables
�metrics
n	variables
 �layer_regularization_losses
 

,0

,0
�
pregularization_losses
�layer_metrics
�non_trainable_variables
�layers
qtrainable_variables
�metrics
r	variables
 �layer_regularization_losses
 

-0

-0
�
tregularization_losses
�layer_metrics
�non_trainable_variables
�layers
utrainable_variables
�metrics
v	variables
 �layer_regularization_losses
 

.0

.0
�
xregularization_losses
�layer_metrics
�non_trainable_variables
�layers
ytrainable_variables
�metrics
z	variables
 �layer_regularization_losses
 
 
 
 
 
 
 
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
 
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
 
ca
VARIABLE_VALUEtrue_positives_1=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_negatives>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
ca
VARIABLE_VALUEtrue_positives_2=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEtrue_negatives=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_1>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_1>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
�0
�1
�2
�3

�	variables
ca
VARIABLE_VALUEtrue_positives_3=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_1=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_2>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_2>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
�0
�1
�2
�3

�	variables
ca
VARIABLE_VALUEtrue_positives_4=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_2=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_3>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_3>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
�0
�1
�2
�3

�	variables
ca
VARIABLE_VALUEtrue_positives_5=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEtrue_negatives_3=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_positives_4>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUE
ec
VARIABLE_VALUEfalse_negatives_4>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUE
 
�0
�1
�2
�3

�	variables
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
��
VARIABLE_VALUEPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/mLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/mLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/mLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/mLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUETAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/mLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/mLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/mLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/vLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/vLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/vLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/vLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUETAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/vLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/vLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/vLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
z
serving_default_input_1Placeholder*'
_output_shapes
:���������)*
dtype0*
shape:���������)
z
serving_default_input_2Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
z
serving_default_input_3Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
z
serving_default_input_4Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
z
serving_default_input_5Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
z
serving_default_input_6Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_1serving_default_input_2serving_default_input_3serving_default_input_4serving_default_input_5serving_default_input_6Imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddingsHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddingsHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddingsNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddingsMmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddingsCmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernelAmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8� *.
f)R'
%__inference_signature_wrapper_5890124
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenameAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOp]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Read/ReadVariableOp\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Read/ReadVariableOp\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Read/ReadVariableOpbmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Read/ReadVariableOpamsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Read/ReadVariableOpWmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/Read/ReadVariableOpUmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOp"true_positives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp$true_positives_1/Read/ReadVariableOp#false_negatives/Read/ReadVariableOp$true_positives_2/Read/ReadVariableOp"true_negatives/Read/ReadVariableOp%false_positives_1/Read/ReadVariableOp%false_negatives_1/Read/ReadVariableOp$true_positives_3/Read/ReadVariableOp$true_negatives_1/Read/ReadVariableOp%false_positives_2/Read/ReadVariableOp%false_negatives_2/Read/ReadVariableOp$true_positives_4/Read/ReadVariableOp$true_negatives_2/Read/ReadVariableOp%false_positives_3/Read/ReadVariableOp%false_negatives_3/Read/ReadVariableOp$true_positives_5/Read/ReadVariableOp$true_negatives_3/Read/ReadVariableOp%false_positives_4/Read/ReadVariableOp%false_negatives_4/Read/ReadVariableOpdAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/m/Read/ReadVariableOpcAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/m/Read/ReadVariableOpcAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/m/Read/ReadVariableOpiAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/m/Read/ReadVariableOphAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/m/Read/ReadVariableOp^Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/m/Read/ReadVariableOp\Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/m/Read/ReadVariableOpdAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/v/Read/ReadVariableOpcAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/v/Read/ReadVariableOpcAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/v/Read/ReadVariableOpiAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/v/Read/ReadVariableOphAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/v/Read/ReadVariableOp^Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/v/Read/ReadVariableOp\Adam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v/Read/ReadVariableOpConst*=
Tin6
422	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *)
f$R"
 __inference__traced_save_5890563
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_rateImsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddingsHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddingsHmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddingsNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddingsMmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddingsCmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernelAmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/biastotalcounttrue_positivesfalse_positivestrue_positives_1false_negativestrue_positives_2true_negativesfalse_positives_1false_negatives_1true_positives_3true_negatives_1false_positives_2false_negatives_2true_positives_4true_negatives_2false_positives_3false_negatives_3true_positives_5true_negatives_3false_positives_4false_negatives_4PAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/mOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/mOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/mUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/mTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/mJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/mHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/mPAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/vOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/vOAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/vUAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/vTAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/vJAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/vHAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v*<
Tin5
321*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference__traced_restore_5890717��

�
A
%__inference_mlp_layer_call_fn_5890266

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_mlp_layer_call_and_return_conditional_losses_58899242
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������3:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs
�
�
__inference_loss_fn_2_5890339�
smsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_regularizer_square_readvariableop_resource:
identity��jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpsmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul�
IdentityIdentity\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul:z:0k^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp
�
�
__inference_loss_fn_3_5890350�
ymsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_regularizer_square_readvariableop_resource:

identity��pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpymsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:
*
dtype02r
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquarexmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumemsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulimsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul�
IdentityIdentitybmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul:z:0q^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOppmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp
�
t
H__inference_concatenate_layer_call_and_return_conditional_losses_5890251
inputs_0
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis�
concatConcatV2inputs_0inputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:���������32
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������):���������
:Q M
'
_output_shapes
:���������)
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������

"
_user_specified_name
inputs/1
�
r
H__inference_concatenate_layer_call_and_return_conditional_losses_5889918

inputs
inputs_1
identity\
concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concat/axis
concatConcatV2inputsinputs_1concat/axis:output:0*
N*
T0*'
_output_shapes
:���������32
concatc
IdentityIdentityconcat:output:0*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������):���������
:O K
'
_output_shapes
:���������)
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������

 
_user_specified_nameinputs
�
\
@__inference_mlp_layer_call_and_return_conditional_losses_5890261

inputs
identityZ
IdentityIdentityinputs*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������3:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs
ϵ
�
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5890225
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4V
Dew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5890162:U
Cew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5890168:U
Cew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5890174:[
Iew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5890180:
Z
Hew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5890186:

identity��<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
0EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/CastCastinputs_0*

DstT0*

SrcT0*'
_output_shapes
:���������22
0EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast�
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookupResourceGatherDew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_58901624EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*W
_classM
KIloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5890162*+
_output_shapes
:���������*
dtype02>
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�
EEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentityEEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*W
_classM
KIloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5890162*+
_output_shapes
:���������2G
EEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity�
GEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1IdentityNEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2I
GEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1�
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/CastCastinputs_1*

DstT0*

SrcT0*'
_output_shapes
:���������21
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast�
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupResourceGatherCew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_58901683EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5890168*+
_output_shapes
:���������*
dtype02=
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentityDEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5890168*+
_output_shapes
:���������2F
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity�
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1IdentityMEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2H
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1�
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/CastCastinputs_2*

DstT0*

SrcT0*'
_output_shapes
:���������21
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast�
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupResourceGatherCew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_58901743EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5890174*+
_output_shapes
:���������*
dtype02=
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentityDEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5890174*+
_output_shapes
:���������2F
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity�
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1IdentityMEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2H
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1�
5EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/CastCastinputs_3*

DstT0*

SrcT0*'
_output_shapes
:���������27
5EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast�
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupResourceGatherIew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_58901809EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*\
_classR
PNloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5890180*+
_output_shapes
:���������*
dtype02C
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�
JEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentityJEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*\
_classR
PNloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5890180*+
_output_shapes
:���������2L
JEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity�
LEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1IdentitySEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2N
LEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1�
4EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/CastCastinputs_4*

DstT0*

SrcT0*'
_output_shapes
:���������26
4EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast�
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupResourceGatherHew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_58901868EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5890186*+
_output_shapes
:���������*
dtype02B
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�
IEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentityIEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5890186*+
_output_shapes
:���������2K
IEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity�
KEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1IdentityREW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2M
KEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1t
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis�
concatenate/concatConcatV2PEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0OEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0OEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0UEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0TEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:0 concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:���������
2
concatenate/concat�
SqueezeSqueezeconcatenate/concat:output:0*
T0*'
_output_shapes
:���������
*
squeeze_dims

���������2	
Squeeze�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpDew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5890162*
_output_shapes

:*
dtype02m
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquaresmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SumSum`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0dmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mulMuldmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpCew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5890168*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpCew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5890174*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpIew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5890180*
_output_shapes

:
*
dtype02r
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquarexmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumemsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulimsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpHew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5890186*
_output_shapes

:
*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul�
IdentityIdentitySqueeze:output:0=^EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup<^EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupA^EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupB^EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup<^EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupl^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpp^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpq^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:���������:���������:���������:���������:���������: : : : : 2|
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup2z
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup2�
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup2�
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupAEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup2z
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup2�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpkmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOppmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/4
�
�
+__inference_msd_model_layer_call_fn_5890019
input_1
input_2
input_3
input_4
input_5
input_6
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

	unknown_4:3M
	unknown_5:M
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1input_2input_3input_4input_5input_6unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_msd_model_layer_call_and_return_conditional_losses_58899942
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������):���������:���������:���������:���������:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:���������)
!
_user_specified_name	input_1:PL
'
_output_shapes
:���������
!
_user_specified_name	input_2:PL
'
_output_shapes
:���������
!
_user_specified_name	input_3:PL
'
_output_shapes
:���������
!
_user_specified_name	input_4:PL
'
_output_shapes
:���������
!
_user_specified_name	input_5:PL
'
_output_shapes
:���������
!
_user_specified_name	input_6
�
�
)__inference_decoder_layer_call_fn_5890306

inputs
unknown:3M
	unknown_0:M
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������M*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_decoder_layer_call_and_return_conditional_losses_58899572
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������3: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs
�
�
__inference_loss_fn_0_5890317�
tmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_regularizer_square_readvariableop_resource:
identity��kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOptmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02m
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquaresmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SumSum`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0dmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mulMuldmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul�
IdentityIdentity]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul:z:0l^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpkmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp
�
�
*__inference_cat2_vec_layer_call_fn_5890244
inputs_0
inputs_1
inputs_2
inputs_3
inputs_4
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1inputs_2inputs_3inputs_4unknown	unknown_0	unknown_1	unknown_2	unknown_3*
Tin
2
*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_cat2_vec_layer_call_and_return_conditional_losses_58898992
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:���������:���������:���������:���������:���������: : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:���������
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/1:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/2:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/3:QM
'
_output_shapes
:���������
"
_user_specified_name
inputs/4
�
Y
-__inference_concatenate_layer_call_fn_5890257
inputs_0
inputs_1
identity�
PartitionedCallPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_concatenate_layer_call_and_return_conditional_losses_58899182
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*9
_input_shapes(
&:���������):���������
:Q M
'
_output_shapes
:���������)
"
_user_specified_name
inputs/0:QM
'
_output_shapes
:���������

"
_user_specified_name
inputs/1
��
�
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5889899

inputs
inputs_1
inputs_2
inputs_3
inputs_4V
Dew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5889836:U
Cew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5889842:U
Cew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5889848:[
Iew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5889854:
Z
Hew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5889860:

identity��<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
0EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/CastCastinputs*

DstT0*

SrcT0*'
_output_shapes
:���������22
0EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast�
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookupResourceGatherDew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_58898364EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*W
_classM
KIloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5889836*+
_output_shapes
:���������*
dtype02>
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�
EEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentityEEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*W
_classM
KIloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5889836*+
_output_shapes
:���������2G
EEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity�
GEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1IdentityNEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2I
GEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1�
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/CastCastinputs_1*

DstT0*

SrcT0*'
_output_shapes
:���������21
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast�
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupResourceGatherCew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_58898423EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5889842*+
_output_shapes
:���������*
dtype02=
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentityDEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5889842*+
_output_shapes
:���������2F
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity�
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1IdentityMEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2H
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1�
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/CastCastinputs_2*

DstT0*

SrcT0*'
_output_shapes
:���������21
/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast�
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupResourceGatherCew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_58898483EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5889848*+
_output_shapes
:���������*
dtype02=
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentityDEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*V
_classL
JHloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5889848*+
_output_shapes
:���������2F
DEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity�
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1IdentityMEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2H
FEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1�
5EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/CastCastinputs_3*

DstT0*

SrcT0*'
_output_shapes
:���������27
5EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast�
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupResourceGatherIew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_58898549EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*\
_classR
PNloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5889854*+
_output_shapes
:���������*
dtype02C
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�
JEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentityJEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*\
_classR
PNloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5889854*+
_output_shapes
:���������2L
JEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity�
LEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1IdentitySEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2N
LEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1�
4EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/CastCastinputs_4*

DstT0*

SrcT0*'
_output_shapes
:���������26
4EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast�
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupResourceGatherHew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_58898608EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5889860*+
_output_shapes
:���������*
dtype02B
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�
IEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentityIEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*[
_classQ
OMloc:@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5889860*+
_output_shapes
:���������2K
IEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity�
KEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1IdentityREW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2M
KEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1t
concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
concatenate/concat/axis�
concatenate/concatConcatV2PEW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0OEW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0OEW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0UEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0TEW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:0 concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:���������
2
concatenate/concat�
SqueezeSqueezeconcatenate/concat:output:0*
T0*'
_output_shapes
:���������
*
squeeze_dims

���������2	
Squeeze�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpDew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5889836*
_output_shapes

:*
dtype02m
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquaresmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SumSum`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0dmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mulMuldmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpCew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5889842*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpCew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5889848*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpIew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5889854*
_output_shapes

:
*
dtype02r
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquarexmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumemsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulimsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpHew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5889860*
_output_shapes

:
*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul�
IdentityIdentitySqueeze:output:0=^EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup<^EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupA^EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupB^EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup<^EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupl^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpp^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpq^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:���������
2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*|
_input_shapesk
i:���������:���������:���������:���������:���������: : : : : 2|
<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup<EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup2z
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup;EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup2�
@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup@EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup2�
AEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupAEW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup2z
;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup;EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup2�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpkmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOppmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs:OK
'
_output_shapes
:���������
 
_user_specified_nameinputs
�s
�
 __inference__traced_save_5890563
file_prefix(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableoph
dsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_read_readvariableopg
csavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_read_readvariableopg
csavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_read_readvariableopm
isavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_read_readvariableopl
hsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_read_readvariableopb
^savev2_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_read_readvariableop`
\savev2_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop-
)savev2_true_positives_read_readvariableop.
*savev2_false_positives_read_readvariableop/
+savev2_true_positives_1_read_readvariableop.
*savev2_false_negatives_read_readvariableop/
+savev2_true_positives_2_read_readvariableop-
)savev2_true_negatives_read_readvariableop0
,savev2_false_positives_1_read_readvariableop0
,savev2_false_negatives_1_read_readvariableop/
+savev2_true_positives_3_read_readvariableop/
+savev2_true_negatives_1_read_readvariableop0
,savev2_false_positives_2_read_readvariableop0
,savev2_false_negatives_2_read_readvariableop/
+savev2_true_positives_4_read_readvariableop/
+savev2_true_negatives_2_read_readvariableop0
,savev2_false_positives_3_read_readvariableop0
,savev2_false_negatives_3_read_readvariableop/
+savev2_true_positives_5_read_readvariableop/
+savev2_true_negatives_3_read_readvariableop0
,savev2_false_positives_4_read_readvariableop0
,savev2_false_negatives_4_read_readvariableopo
ksavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_m_read_readvariableopn
jsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_m_read_readvariableopn
jsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_m_read_readvariableopt
psavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_m_read_readvariableops
osavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_m_read_readvariableopi
esavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_m_read_readvariableopg
csavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_m_read_readvariableopo
ksavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_v_read_readvariableopn
jsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_v_read_readvariableopn
jsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_v_read_readvariableopt
psavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_v_read_readvariableops
osavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_v_read_readvariableopi
esavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_v_read_readvariableopg
csavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_v_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*�
value�B�1B)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*u
valuelBj1B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableopdsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_read_readvariableopcsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_read_readvariableopcsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_read_readvariableopisavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_read_readvariableophsavev2_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_read_readvariableop^savev2_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_read_readvariableop\savev2_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop)savev2_true_positives_read_readvariableop*savev2_false_positives_read_readvariableop+savev2_true_positives_1_read_readvariableop*savev2_false_negatives_read_readvariableop+savev2_true_positives_2_read_readvariableop)savev2_true_negatives_read_readvariableop,savev2_false_positives_1_read_readvariableop,savev2_false_negatives_1_read_readvariableop+savev2_true_positives_3_read_readvariableop+savev2_true_negatives_1_read_readvariableop,savev2_false_positives_2_read_readvariableop,savev2_false_negatives_2_read_readvariableop+savev2_true_positives_4_read_readvariableop+savev2_true_negatives_2_read_readvariableop,savev2_false_positives_3_read_readvariableop,savev2_false_negatives_3_read_readvariableop+savev2_true_positives_5_read_readvariableop+savev2_true_negatives_3_read_readvariableop,savev2_false_positives_4_read_readvariableop,savev2_false_negatives_4_read_readvariableopksavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_m_read_readvariableopjsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_m_read_readvariableopjsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_m_read_readvariableoppsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_m_read_readvariableoposavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_m_read_readvariableopesavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_m_read_readvariableopcsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_m_read_readvariableopksavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_v_read_readvariableopjsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_v_read_readvariableopjsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_v_read_readvariableoppsavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_v_read_readvariableoposavev2_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_v_read_readvariableopesavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_v_read_readvariableopcsavev2_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_v_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *?
dtypes5
321	2
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identitym

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: : : : : : ::::
:
:3M:M: : :::::	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M:	�M::::
:
:3M:M::::
:
:3M:M: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :$ 

_output_shapes

::$ 

_output_shapes

::$ 

_output_shapes

::$	 

_output_shapes

:
:$
 

_output_shapes

:
:$ 

_output_shapes

:3M: 

_output_shapes
:M:

_output_shapes
: :

_output_shapes
: : 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
:: 

_output_shapes
::%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:%!

_output_shapes
:	�M:% !

_output_shapes
:	�M:%!!

_output_shapes
:	�M:%"!

_output_shapes
:	�M:$# 

_output_shapes

::$$ 

_output_shapes

::$% 

_output_shapes

::$& 

_output_shapes

:
:$' 

_output_shapes

:
:$( 

_output_shapes

:3M: )

_output_shapes
:M:$* 

_output_shapes

::$+ 

_output_shapes

::$, 

_output_shapes

::$- 

_output_shapes

:
:$. 

_output_shapes

:
:$/ 

_output_shapes

:3M: 0

_output_shapes
:M:1

_output_shapes
: 
�
�
%__inference_signature_wrapper_5890124
input_1
input_2
input_3
input_4
input_5
input_6
unknown:
	unknown_0:
	unknown_1:
	unknown_2:

	unknown_3:

	unknown_4:3M
	unknown_5:M
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_1input_2input_3input_4input_5input_6unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������M*)
_read_only_resource_inputs
		
*-
config_proto

CPU

GPU 2J 8� *+
f&R$
"__inference__wrapped_model_58898192
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������):���������:���������:���������:���������:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
'
_output_shapes
:���������)
!
_user_specified_name	input_1:PL
'
_output_shapes
:���������
!
_user_specified_name	input_2:PL
'
_output_shapes
:���������
!
_user_specified_name	input_3:PL
'
_output_shapes
:���������
!
_user_specified_name	input_4:PL
'
_output_shapes
:���������
!
_user_specified_name	input_5:PL
'
_output_shapes
:���������
!
_user_specified_name	input_6
�U
�
D__inference_decoder_layer_call_and_return_conditional_losses_5889957

inputs[
Iew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource:3MX
Jew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource:M
identity��AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOpReadVariableOpIew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02B
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
1EW_avg_embed_triangle_2_fl2_decoder_output/MatMulMatMulinputsHEW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M23
1EW_avg_embed_triangle_2_fl2_decoder_output/MatMul�
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOpJew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02C
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�
2EW_avg_embed_triangle_2_fl2_decoder_output/BiasAddBiasAdd;EW_avg_embed_triangle_2_fl2_decoder_output/MatMul:product:0IEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M24
2EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd�
5EW_avg_embed_triangle_2_fl2_decoder_output/Normal/locConst*
_output_shapes
: *
dtype0*
valueB
 *    27
5EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc�
7EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scaleConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale�
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/subSub;EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd:output:0>EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc:output:0*
T0*'
_output_shapes
:���������M2r
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub�
tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truedivRealDivtEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub:z:0@EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale:output:0*
T0*'
_output_shapes
:���������M2v
tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv�
qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2Const*
_output_shapes
: *
dtype0*
valueB
 *�5?2s
qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mulMulxEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv:z:0zEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/AbsAbsmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs�
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/LessLessmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0zEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2l
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfErfmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/addAddV2tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x:output:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf:y:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add�
oEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2q
oEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y�
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/GreaterGreatermEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0xEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y:output:0*
T0*'
_output_shapes
:���������M2o
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater�
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfcErfcmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2l
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *   @2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/subSubtEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x:output:0nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc:y:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub�
lEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1ErfcmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2n
lEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1�
nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2SelectV2qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater:z:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub:z:0pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1:y:0*
T0*'
_output_shapes
:���������M2p
nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2�
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1SelectV2nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less:z:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add:z:0wEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2:output:0*
T0*'
_output_shapes
:���������M2r
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1�
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2o
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1MulvEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x:output:0yEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1:output:0*
T0*'
_output_shapes
:���������M2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1�
IdentityIdentityoEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1:z:0B^EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpA^EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������3: : 2�
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpAEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp2�
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs
��
�
"__inference__wrapped_model_5889819
input_1
input_2
input_3
input_4
input_5
input_6i
Wmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5889757:h
Vmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5889763:h
Vmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5889769:n
\msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5889775:
m
[msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5889781:
m
[msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource:3Mj
\msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource:M
identity��Omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�Smsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�Tmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�Smsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�Rmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
Cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/CastCastinput_2*

DstT0*

SrcT0*'
_output_shapes
:���������2E
Cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast�
Omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookupResourceGatherWmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embedding_lookup_5889757Gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*j
_class`
^\loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5889757*+
_output_shapes
:���������*
dtype02Q
Omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/IdentityIdentityXmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*j
_class`
^\loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/5889757*+
_output_shapes
:���������2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1Identityamsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1�
Bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/CastCastinput_3*

DstT0*

SrcT0*'
_output_shapes
:���������2D
Bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast�
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupResourceGatherVmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embedding_lookup_5889763Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*i
_class_
][loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5889763*+
_output_shapes
:���������*
dtype02P
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup�
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/IdentityIdentityWmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*i
_class_
][loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/5889763*+
_output_shapes
:���������2Y
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1Identity`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1�
Bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/CastCastinput_4*

DstT0*

SrcT0*'
_output_shapes
:���������2D
Bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast�
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupResourceGatherVmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embedding_lookup_5889769Fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*i
_class_
][loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5889769*+
_output_shapes
:���������*
dtype02P
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup�
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/IdentityIdentityWmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*i
_class_
][loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/5889769*+
_output_shapes
:���������2Y
Wmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1Identity`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1�
Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/CastCastinput_5*

DstT0*

SrcT0*'
_output_shapes
:���������2J
Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast�
Tmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupResourceGather\msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embedding_lookup_5889775Lmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*o
_classe
caloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5889775*+
_output_shapes
:���������*
dtype02V
Tmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/IdentityIdentity]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*o
_classe
caloc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/5889775*+
_output_shapes
:���������2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1Identityfmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1�
Gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/CastCastinput_6*

DstT0*

SrcT0*'
_output_shapes
:���������2I
Gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast�
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupResourceGather[msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embedding_lookup_5889781Kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/Cast:y:0",/job:localhost/replica:0/task:0/device:CPU:0*
Tindices0*n
_classd
b`loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5889781*+
_output_shapes
:���������*
dtype02U
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/IdentityIdentity\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup:output:0",/job:localhost/replica:0/task:0/device:CPU:0*
T0*n
_classd
b`loc:@msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/5889781*+
_output_shapes
:���������2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1Identityemsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity:output:0*
T0*+
_output_shapes
:���������2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1�
*msd_model/cat2_vec/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2,
*msd_model/cat2_vec/concatenate/concat/axis�
%msd_model/cat2_vec/concatenate/concatConcatV2cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup/Identity_1:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup/Identity_1:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup/Identity_1:output:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup/Identity_1:output:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup/Identity_1:output:03msd_model/cat2_vec/concatenate/concat/axis:output:0*
N*
T0*+
_output_shapes
:���������
2'
%msd_model/cat2_vec/concatenate/concat�
msd_model/cat2_vec/SqueezeSqueeze.msd_model/cat2_vec/concatenate/concat:output:0*
T0*'
_output_shapes
:���������
*
squeeze_dims

���������2
msd_model/cat2_vec/Squeeze�
!msd_model/concatenate/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2#
!msd_model/concatenate/concat/axis�
msd_model/concatenate/concatConcatV2input_1#msd_model/cat2_vec/Squeeze:output:0*msd_model/concatenate/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������32
msd_model/concatenate/concat�
Rmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOpReadVariableOp[msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02T
Rmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
Cmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMulMatMul%msd_model/concatenate/concat:output:0Zmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M2E
Cmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul�
Smsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOp\msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02U
Smsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�
Dmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAddBiasAddMmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul:product:0[msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M2F
Dmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd�
Gmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/locConst*
_output_shapes
: *
dtype0*
valueB
 *    2I
Gmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc�
Imsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scaleConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2K
Imsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/subSubMmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd:output:0Pmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truedivRealDiv�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub:z:0Rmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2Const*
_output_shapes
: *
dtype0*
valueB
 *�5?2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mulMul�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/AbsAbs�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/LessLess�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfErf�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/addAddV2�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x:output:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf:y:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/GreaterGreater�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfcErfc�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *   @2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/subSub�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x:output:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc:y:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1Erfc�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2SelectV2�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1:y:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1SelectV2�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add:z:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1Mul�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x:output:0�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1:output:0*
T0*'
_output_shapes
:���������M2�
�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1�
IdentityIdentity�msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/msd_model_decoder_EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1:z:0P^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookupO^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupT^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupU^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupO^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupT^msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpS^msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������):���������:���������:���������:���������:���������: : : : : : : 2�
Omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookupOmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embedding_lookup2�
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookupNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embedding_lookup2�
Smsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookupSmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embedding_lookup2�
Tmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookupTmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embedding_lookup2�
Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookupNmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embedding_lookup2�
Smsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpSmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp2�
Rmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOpRmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:P L
'
_output_shapes
:���������)
!
_user_specified_name	input_1:PL
'
_output_shapes
:���������
!
_user_specified_name	input_2:PL
'
_output_shapes
:���������
!
_user_specified_name	input_3:PL
'
_output_shapes
:���������
!
_user_specified_name	input_4:PL
'
_output_shapes
:���������
!
_user_specified_name	input_5:PL
'
_output_shapes
:���������
!
_user_specified_name	input_6
��
�&
#__inference__traced_restore_5890717
file_prefix$
assignvariableop_adam_iter:	 (
assignvariableop_1_adam_beta_1: (
assignvariableop_2_adam_beta_2: '
assignvariableop_3_adam_decay: /
%assignvariableop_4_adam_learning_rate: n
\assignvariableop_5_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings:m
[assignvariableop_6_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings:m
[assignvariableop_7_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings:s
aassignvariableop_8_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings:
r
`assignvariableop_9_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings:
i
Wassignvariableop_10_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel:3Mc
Uassignvariableop_11_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias:M#
assignvariableop_12_total: #
assignvariableop_13_count: 0
"assignvariableop_14_true_positives:1
#assignvariableop_15_false_positives:2
$assignvariableop_16_true_positives_1:1
#assignvariableop_17_false_negatives:7
$assignvariableop_18_true_positives_2:	�M5
"assignvariableop_19_true_negatives:	�M8
%assignvariableop_20_false_positives_1:	�M8
%assignvariableop_21_false_negatives_1:	�M7
$assignvariableop_22_true_positives_3:	�M7
$assignvariableop_23_true_negatives_1:	�M8
%assignvariableop_24_false_positives_2:	�M8
%assignvariableop_25_false_negatives_2:	�M7
$assignvariableop_26_true_positives_4:	�M7
$assignvariableop_27_true_negatives_2:	�M8
%assignvariableop_28_false_positives_3:	�M8
%assignvariableop_29_false_negatives_3:	�M7
$assignvariableop_30_true_positives_5:	�M7
$assignvariableop_31_true_negatives_3:	�M8
%assignvariableop_32_false_positives_4:	�M8
%assignvariableop_33_false_negatives_4:	�Mv
dassignvariableop_34_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_m:u
cassignvariableop_35_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_m:u
cassignvariableop_36_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_m:{
iassignvariableop_37_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_m:
z
hassignvariableop_38_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_m:
p
^assignvariableop_39_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_m:3Mj
\assignvariableop_40_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_m:Mv
dassignvariableop_41_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_v:u
cassignvariableop_42_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_v:u
cassignvariableop_43_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_v:{
iassignvariableop_44_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_v:
z
hassignvariableop_45_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_v:
p
^assignvariableop_46_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_v:3Mj
\assignvariableop_47_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_v:M
identity_49��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*�
value�B�1B)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/0/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/1/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/2/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/3/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/4/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/5/.ATTRIBUTES/VARIABLE_VALUEB0trainable_variables/6/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/1/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/1/false_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/3/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/3/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/4/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/4/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/5/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/5/false_negatives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_positives/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/6/true_negatives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/6/false_negatives/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/0/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/1/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/2/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/3/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/4/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/5/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBLtrainable_variables/6/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:1*
dtype0*u
valuelBj1B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::::::::::*?
dtypes5
321	2
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0	*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_adam_iterIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	2
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_adam_beta_1Identity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOpassignvariableop_2_adam_beta_2Identity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_decayIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp%assignvariableop_4_adam_learning_rateIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp\assignvariableop_5_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddingsIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp[assignvariableop_6_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddingsIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp[assignvariableop_7_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddingsIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOpaassignvariableop_8_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddingsIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp`assignvariableop_9_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddingsIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOpWassignvariableop_10_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernelIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpUassignvariableop_11_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_biasIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOpassignvariableop_12_totalIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOpassignvariableop_13_countIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_13n
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp"assignvariableop_14_true_positivesIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_14n
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp#assignvariableop_15_false_positivesIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_15n
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp$assignvariableop_16_true_positives_1Identity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_16n
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp#assignvariableop_17_false_negativesIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_17n
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOp$assignvariableop_18_true_positives_2Identity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_18n
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOp"assignvariableop_19_true_negativesIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_19n
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOp%assignvariableop_20_false_positives_1Identity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_20n
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOp%assignvariableop_21_false_negatives_1Identity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_21n
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp$assignvariableop_22_true_positives_3Identity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_22n
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOp$assignvariableop_23_true_negatives_1Identity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_23n
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOp%assignvariableop_24_false_positives_2Identity_24:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_24n
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOp%assignvariableop_25_false_negatives_2Identity_25:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_25n
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOp$assignvariableop_26_true_positives_4Identity_26:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_26n
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp$assignvariableop_27_true_negatives_2Identity_27:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_27n
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp%assignvariableop_28_false_positives_3Identity_28:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_28n
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp%assignvariableop_29_false_negatives_3Identity_29:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_29n
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp$assignvariableop_30_true_positives_5Identity_30:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_30n
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp$assignvariableop_31_true_negatives_3Identity_31:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_31n
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp%assignvariableop_32_false_positives_4Identity_32:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_32n
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp%assignvariableop_33_false_negatives_4Identity_33:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_33n
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOpdassignvariableop_34_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_mIdentity_34:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_34n
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOpcassignvariableop_35_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_mIdentity_35:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_35n
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOpcassignvariableop_36_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_mIdentity_36:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_36n
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOpiassignvariableop_37_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_mIdentity_37:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_37n
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOphassignvariableop_38_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_mIdentity_38:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_38n
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOp^assignvariableop_39_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_mIdentity_39:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_39n
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOp\assignvariableop_40_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_mIdentity_40:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_40n
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOpdassignvariableop_41_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_clc_embeddings_vIdentity_41:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_41n
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOpcassignvariableop_42_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_vIdentity_42:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_42n
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOpcassignvariableop_43_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_hg_embeddings_vIdentity_43:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_43n
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOpiassignvariableop_44_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1_embeddings_vIdentity_44:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_44n
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOphassignvariableop_45_adam_msd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_vIdentity_45:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_45n
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp^assignvariableop_46_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_kernel_vIdentity_46:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_46n
Identity_47IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:2
Identity_47�
AssignVariableOp_47AssignVariableOp\assignvariableop_47_adam_msd_model_decoder_ew_avg_embed_triangle_2_fl2_decoder_output_bias_vIdentity_47:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_479
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_48Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_48�
Identity_49IdentityIdentity_48:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
T0*
_output_shapes
: 2
Identity_49"#
identity_49Identity_49:output:0*u
_input_shapesd
b: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�{
�
F__inference_msd_model_layer_call_and_return_conditional_losses_5889994
input_1
input_2
input_3
input_4
input_5
input_6"
cat2_vec_5889900:"
cat2_vec_5889902:"
cat2_vec_5889904:"
cat2_vec_5889906:
"
cat2_vec_5889908:
!
decoder_5889958:3M
decoder_5889960:M
identity�� cat2_vec/StatefulPartitionedCall�decoder/StatefulPartitionedCall�kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
 cat2_vec/StatefulPartitionedCallStatefulPartitionedCallinput_2input_3input_4input_5input_6cat2_vec_5889900cat2_vec_5889902cat2_vec_5889904cat2_vec_5889906cat2_vec_5889908*
Tin
2
*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_cat2_vec_layer_call_and_return_conditional_losses_58898992"
 cat2_vec/StatefulPartitionedCall�
concatenate/PartitionedCallPartitionedCallinput_1)cat2_vec/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *Q
fLRJ
H__inference_concatenate_layer_call_and_return_conditional_losses_58899182
concatenate/PartitionedCall�
mlp/PartitionedCallPartitionedCall$concatenate/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������3* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_mlp_layer_call_and_return_conditional_losses_58899242
mlp/PartitionedCall�
decoder/StatefulPartitionedCallStatefulPartitionedCallmlp/PartitionedCall:output:0decoder_5889958decoder_5889960*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������M*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_decoder_layer_call_and_return_conditional_losses_58899572!
decoder/StatefulPartitionedCall�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5889900*
_output_shapes

:*
dtype02m
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp�
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SquareSquaresmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2^
\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/SumSum`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square:y:0dmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x�
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mulMuldmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul/x:output:0bmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2[
Ymsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5889902*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5889904*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/mul�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5889906*
_output_shapes

:
*
dtype02r
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp�
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SquareSquarexmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2c
amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/SumSumemsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square:y:0imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x�
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mulMulimsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul/x:output:0gmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2`
^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/mul�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpcat2_vec_5889908*
_output_shapes

:
*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul�
IdentityIdentity(decoder/StatefulPartitionedCall:output:0!^cat2_vec/StatefulPartitionedCall ^decoder/StatefulPartitionedCalll^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpp^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpq^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOpk^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*�
_input_shapes�
�:���������):���������:���������:���������:���������:���������: : : : : : : 2D
 cat2_vec/StatefulPartitionedCall cat2_vec/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2�
kmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOpkmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/Regularizer/Square/ReadVariableOp2�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp2�
pmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOppmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/Regularizer/Square/ReadVariableOp2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:P L
'
_output_shapes
:���������)
!
_user_specified_name	input_1:PL
'
_output_shapes
:���������
!
_user_specified_name	input_2:PL
'
_output_shapes
:���������
!
_user_specified_name	input_3:PL
'
_output_shapes
:���������
!
_user_specified_name	input_4:PL
'
_output_shapes
:���������
!
_user_specified_name	input_5:PL
'
_output_shapes
:���������
!
_user_specified_name	input_6
�
�
__inference_loss_fn_4_5890361�
xmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_regularizer_square_readvariableop_resource:

identity��omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpxmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_min_top_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:
*
dtype02q
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp�
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SquareSquarewmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:
2b
`msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/SumSumdmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square:y:0hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum�
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2a
_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x�
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mulMulhmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul/x:output:0fmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2_
]msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul�
IdentityIdentityamsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/mul:z:0p^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2�
omsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOpomsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/Regularizer/Square/ReadVariableOp
�
�
__inference_loss_fn_1_5890328�
smsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_regularizer_square_readvariableop_resource:
identity��jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpReadVariableOpsmsd_model_cat2_vec_ew_avg_embed_triangle_2_fl2_cat2vec_emb_wr_embeddings_regularizer_square_readvariableop_resource*
_output_shapes

:*
dtype02l
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp�
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SquareSquarermsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp:value:0*
T0*
_output_shapes

:2]
[msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/SumSum_msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square:y:0cmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Const:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum�
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *
�#<2\
Zmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x�
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mulMulcmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul/x:output:0amsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Sum:output:0*
T0*
_output_shapes
: 2Z
Xmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul�
IdentityIdentity\msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/mul:z:0k^msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp*
T0*
_output_shapes
: 2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2�
jmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOpjmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/Regularizer/Square/ReadVariableOp
�
\
@__inference_mlp_layer_call_and_return_conditional_losses_5889924

inputs
identityZ
IdentityIdentityinputs*
T0*'
_output_shapes
:���������32

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������3:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs
�U
�
D__inference_decoder_layer_call_and_return_conditional_losses_5890297

inputs[
Iew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource:3MX
Jew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource:M
identity��AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOpReadVariableOpIew_avg_embed_triangle_2_fl2_decoder_output_matmul_readvariableop_resource*
_output_shapes

:3M*
dtype02B
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp�
1EW_avg_embed_triangle_2_fl2_decoder_output/MatMulMatMulinputsHEW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M23
1EW_avg_embed_triangle_2_fl2_decoder_output/MatMul�
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpReadVariableOpJew_avg_embed_triangle_2_fl2_decoder_output_biasadd_readvariableop_resource*
_output_shapes
:M*
dtype02C
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp�
2EW_avg_embed_triangle_2_fl2_decoder_output/BiasAddBiasAdd;EW_avg_embed_triangle_2_fl2_decoder_output/MatMul:product:0IEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������M24
2EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd�
5EW_avg_embed_triangle_2_fl2_decoder_output/Normal/locConst*
_output_shapes
: *
dtype0*
valueB
 *    27
5EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc�
7EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scaleConst*
_output_shapes
: *
dtype0*
valueB
 *  �?29
7EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale�
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/subSub;EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd:output:0>EW_avg_embed_triangle_2_fl2_decoder_output/Normal/loc:output:0*
T0*'
_output_shapes
:���������M2r
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub�
tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truedivRealDivtEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/sub:z:0@EW_avg_embed_triangle_2_fl2_decoder_output/Normal/scale:output:0*
T0*'
_output_shapes
:���������M2v
tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv�
qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2Const*
_output_shapes
: *
dtype0*
valueB
 *�5?2s
qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mulMulxEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/standardize/truediv:z:0zEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/AbsAbsmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs�
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/LessLessmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0zEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/half_sqrt_2:output:0*
T0*'
_output_shapes
:���������M2l
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfErfmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/addAddV2tEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add/x:output:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erf:y:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add�
oEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2q
oEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y�
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/GreaterGreatermEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul:z:0xEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater/y:output:0*
T0*'
_output_shapes
:���������M2o
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater�
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/ErfcErfcmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2l
jEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/xConst*
_output_shapes
: *
dtype0*
valueB
 *   @2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x�
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/subSubtEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub/x:output:0nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc:y:0*
T0*'
_output_shapes
:���������M2k
iEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub�
lEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1ErfcmEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Abs:y:0*
T0*'
_output_shapes
:���������M2n
lEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1�
nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2SelectV2qEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Greater:z:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/sub:z:0pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Erfc_1:y:0*
T0*'
_output_shapes
:���������M2p
nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2�
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1SelectV2nEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/Less:z:0mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/add:z:0wEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2:output:0*
T0*'
_output_shapes
:���������M2r
pEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1�
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/xConst*
_output_shapes
: *
dtype0*
valueB
 *   ?2o
mEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x�
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1MulvEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1/x:output:0yEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/SelectV2_1:output:0*
T0*'
_output_shapes
:���������M2m
kEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1�
IdentityIdentityoEW_avg_embed_triangle_2_fl2_decoder_output/EW_avg_embed_triangle_2_fl2_decoder_output_Normal/cdf/ndtr/mul_1:z:0B^EW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpA^EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp*
T0*'
_output_shapes
:���������M2

Identity"
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������3: : 2�
AEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOpAEW_avg_embed_triangle_2_fl2_decoder_output/BiasAdd/ReadVariableOp2�
@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp@EW_avg_embed_triangle_2_fl2_decoder_output/MatMul/ReadVariableOp:O K
'
_output_shapes
:���������3
 
_user_specified_nameinputs"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
;
input_10
serving_default_input_1:0���������)
;
input_20
serving_default_input_2:0���������
;
input_30
serving_default_input_3:0���������
;
input_40
serving_default_input_4:0���������
;
input_50
serving_default_input_5:0���������
;
input_60
serving_default_input_6:0���������<
output_10
StatefulPartitionedCall:0���������Mtensorflow/serving/predict:��
ӱ
numvars
catvars
	archi
cat2vec
fconcat
fe
response
	optimizer
	regularization_losses

trainable_variables
	variables
	keras_api

signatures
+�&call_and_return_all_conditional_losses
�_default_save_signature
�__call__"��
_tf_keras_model��{"name": "msd_model", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "must_restore_from_config": false, "class_name": "MSDModel", "config": {"layer was saved without config": true}, "is_graph_network": false, "save_spec": {"class_name": "__tuple__", "items": [{"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 41]}, "float32", "input_1"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_2"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_3"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_4"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_5"]}, {"class_name": "TypeSpec", "type_spec": "tf.TensorSpec", "serialized": [{"class_name": "TensorShape", "items": [null, 1]}, "float32", "input_6"]}]}, "keras_version": "2.5.0", "backend": "tensorflow", "model_config": {"class_name": "MSDModel"}, "training_config": {"loss": {"class_name": "Custom>BinaryFocalLoss", "config": {"reduction": "auto", "name": null, "gamma": 2.0, "pos_weight": null, "from_logits": false, "label_smoothing": null}, "shared_object_id": 0}, "metrics": [[{"class_name": "Precision", "config": {"name": "precision", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 1}, {"class_name": "Recall", "config": {"name": "recall", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 2}, {"class_name": "AUC", "config": {"name": "rocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 3}, {"class_name": "AUC", "config": {"name": "prauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 4}, {"class_name": "AUC", "config": {"name": "wrocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 5}, {"class_name": "AUC", "config": {"name": "wprauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 6}]], "weighted_metrics": null, "loss_weights": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0010000000474974513, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
 "
trackable_list_wrapper
 "
trackable_list_wrapper
3
	embed
fe"
trackable_dict_wrapper
�

config
embed_layers
regularization_losses
trainable_variables
	variables
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "cat2_vec", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Cat2Vec", "config": {"layer was saved without config": true}}
�
regularization_losses
trainable_variables
	variables
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "concatenate", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Concatenate", "config": {"name": "concatenate", "trainable": true, "dtype": "float32", "axis": -1}, "shared_object_id": 7, "build_input_shape": [{"class_name": "TensorShape", "items": [null, 41]}, {"class_name": "TensorShape", "items": [null, 10]}]}
�

config

layers
regularization_losses
trainable_variables
	variables
	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "mlp", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "MLP", "config": {"layer was saved without config": true}}
�

config
fe
 
prediction
!regularization_losses
"trainable_variables
#	variables
$	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "decoder", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Decoder", "config": {"layer was saved without config": true}}
�
%iter

&beta_1

'beta_2
	(decay
)learning_rate*m�+m�,m�-m�.m�/m�0m�*v�+v�,v�-v�.v�/v�0v�"
	optimizer
 "
trackable_list_wrapper
Q
*0
+1
,2
-3
.4
/5
06"
trackable_list_wrapper
Q
*0
+1
,2
-3
.4
/5
06"
trackable_list_wrapper
�
	regularization_losses
1layer_metrics
2non_trainable_variables

3layers

trainable_variables
4metrics
	variables
5layer_regularization_losses
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
.
6features"
trackable_dict_wrapper
;
	7archi
8
activation"
trackable_dict_wrapper
C
90
:1
;2
<3
=4"
trackable_list_wrapper
H
�0
�1
�2
�3
�4"
trackable_list_wrapper
C
*0
+1
,2
-3
.4"
trackable_list_wrapper
C
*0
+1
,2
-3
.4"
trackable_list_wrapper
�
regularization_losses
>layer_metrics
?non_trainable_variables

@layers
trainable_variables
Ametrics
	variables
Blayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
regularization_losses
Clayer_metrics
Dnon_trainable_variables

Elayers
trainable_variables
Fmetrics
	variables
Glayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
regularization_losses
Hlayer_metrics
Inon_trainable_variables

Jlayers
trainable_variables
Kmetrics
	variables
Llayer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
)
Mmlp"
trackable_dict_wrapper
�

Mconfig

Nlayers
Oregularization_losses
Ptrainable_variables
Q	variables
R	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "mlp_1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "MLP", "config": {"layer was saved without config": true}}
�

/kernel
0bias
Sregularization_losses
Ttrainable_variables
U	variables
V	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_decoder_output", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "must_restore_from_config": false, "class_name": "Dense", "config": {"name": "EW_avg_embed_triangle_2_fl2_decoder_output", "trainable": true, "dtype": "float32", "units": 77, "activation": "probit", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}, "shared_object_id": 8}, "bias_initializer": "my_bias_init", "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "shared_object_id": 9, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 51}}, "shared_object_id": 10}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 51]}}
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
�
!regularization_losses
Wlayer_metrics
Xnon_trainable_variables

Ylayers
"trainable_variables
Zmetrics
#	variables
[layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
[:Y2Imsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings
Z:X2Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings
Z:X2Hmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings
`:^
2Nmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings
_:]
2Mmsd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings
U:S3M2Cmsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel
O:MM2Amsd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
Q
\0
]1
^2
_3
`4
a5
b6"
trackable_list_wrapper
 "
trackable_list_wrapper
C
c0
d1
e2
f3
g4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
*
embeddings
hregularization_losses
itrainable_variables
j	variables
k	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 16, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 11}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 12}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 13, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
�
+
embeddings
lregularization_losses
mtrainable_variables
n	variables
o	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 5, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 14}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 15}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 16, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
�
,
embeddings
pregularization_losses
qtrainable_variables
r	variables
s	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 11, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 17}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 18}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 19, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
�
-
embeddings
tregularization_losses
utrainable_variables
v	variables
w	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 10, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 20}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 21}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 22, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
�
.
embeddings
xregularization_losses
ytrainable_variables
z	variables
{	keras_api
+�&call_and_return_all_conditional_losses
�__call__"�
_tf_keras_layer�{"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "stateful": false, "must_restore_from_config": false, "class_name": "Embedding", "config": {"name": "EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, null]}, "dtype": "float32", "input_dim": 10, "output_dim": 2, "embeddings_initializer": {"class_name": "RandomUniform", "config": {"minval": -0.05, "maxval": 0.05, "seed": null}, "shared_object_id": 23}, "embeddings_regularizer": {"class_name": "L2", "config": {"l2": 0.009999999776482582}, "shared_object_id": 24}, "activity_regularizer": null, "embeddings_constraint": null, "mask_zero": false, "input_length": null}, "shared_object_id": 25, "build_input_shape": {"class_name": "TensorShape", "items": [null, 1]}}
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
C
90
:1
;2
<3
=4"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
;
	|archi
}
activation"
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Oregularization_losses
~layer_metrics
non_trainable_variables
�layers
Ptrainable_variables
�metrics
Q	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
�
Sregularization_losses
�layer_metrics
�non_trainable_variables
�layers
Ttrainable_variables
�metrics
U	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�

�total

�count
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}, "shared_object_id": 26}
�
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "Precision", "name": "precision", "dtype": "float32", "config": {"name": "precision", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 1}
�
�
thresholds
�true_positives
�false_negatives
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "Recall", "name": "recall", "dtype": "float32", "config": {"name": "recall", "dtype": "float32", "thresholds": 0.5, "top_k": null, "class_id": null}, "shared_object_id": 2}
�#
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api"�"
_tf_keras_metric�"{"class_name": "AUC", "name": "rocauc", "dtype": "float32", "config": {"name": "rocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 3, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
�#
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api"�"
_tf_keras_metric�"{"class_name": "AUC", "name": "prauc", "dtype": "float32", "config": {"name": "prauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": null}, "shared_object_id": 4, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
�0
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api"�/
_tf_keras_metric�/{"class_name": "AUC", "name": "wrocauc", "dtype": "float32", "config": {"name": "wrocauc", "dtype": "float32", "num_thresholds": 200, "curve": "ROC", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 5, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
�0
�true_positives
�true_negatives
�false_positives
�false_negatives
�	variables
�	keras_api"�/
_tf_keras_metric�/{"class_name": "AUC", "name": "wprauc", "dtype": "float32", "config": {"name": "wprauc", "dtype": "float32", "num_thresholds": 200, "curve": "PR", "summation_method": "interpolation", "thresholds": [0.005025125628140704, 0.010050251256281407, 0.01507537688442211, 0.020100502512562814, 0.02512562814070352, 0.03015075376884422, 0.035175879396984924, 0.04020100502512563, 0.04522613065326633, 0.05025125628140704, 0.05527638190954774, 0.06030150753768844, 0.06532663316582915, 0.07035175879396985, 0.07537688442211055, 0.08040201005025126, 0.08542713567839195, 0.09045226130653267, 0.09547738693467336, 0.10050251256281408, 0.10552763819095477, 0.11055276381909548, 0.11557788944723618, 0.12060301507537688, 0.12562814070351758, 0.1306532663316583, 0.135678391959799, 0.1407035175879397, 0.1457286432160804, 0.1507537688442211, 0.15577889447236182, 0.16080402010050251, 0.1658291457286432, 0.1708542713567839, 0.17587939698492464, 0.18090452261306533, 0.18592964824120603, 0.19095477386934673, 0.19597989949748743, 0.20100502512562815, 0.20603015075376885, 0.21105527638190955, 0.21608040201005024, 0.22110552763819097, 0.22613065326633167, 0.23115577889447236, 0.23618090452261306, 0.24120603015075376, 0.24623115577889448, 0.25125628140703515, 0.2562814070351759, 0.2613065326633166, 0.2663316582914573, 0.271356783919598, 0.27638190954773867, 0.2814070351758794, 0.2864321608040201, 0.2914572864321608, 0.2964824120603015, 0.3015075376884422, 0.3065326633165829, 0.31155778894472363, 0.3165829145728643, 0.32160804020100503, 0.32663316582914576, 0.3316582914572864, 0.33668341708542715, 0.3417085427135678, 0.34673366834170855, 0.35175879396984927, 0.35678391959798994, 0.36180904522613067, 0.36683417085427134, 0.37185929648241206, 0.3768844221105528, 0.38190954773869346, 0.3869346733668342, 0.39195979899497485, 0.3969849246231156, 0.4020100502512563, 0.40703517587939697, 0.4120603015075377, 0.41708542713567837, 0.4221105527638191, 0.4271356783919598, 0.4321608040201005, 0.4371859296482412, 0.44221105527638194, 0.4472361809045226, 0.45226130653266333, 0.457286432160804, 0.4623115577889447, 0.46733668341708545, 0.4723618090452261, 0.47738693467336685, 0.4824120603015075, 0.48743718592964824, 0.49246231155778897, 0.49748743718592964, 0.5025125628140703, 0.507537688442211, 0.5125628140703518, 0.5175879396984925, 0.5226130653266332, 0.5276381909547738, 0.5326633165829145, 0.5376884422110553, 0.542713567839196, 0.5477386934673367, 0.5527638190954773, 0.5577889447236181, 0.5628140703517588, 0.5678391959798995, 0.5728643216080402, 0.5778894472361809, 0.5829145728643216, 0.5879396984924623, 0.592964824120603, 0.5979899497487438, 0.6030150753768844, 0.6080402010050251, 0.6130653266331658, 0.6180904522613065, 0.6231155778894473, 0.628140703517588, 0.6331658291457286, 0.6381909547738693, 0.6432160804020101, 0.6482412060301508, 0.6532663316582915, 0.6582914572864321, 0.6633165829145728, 0.6683417085427136, 0.6733668341708543, 0.678391959798995, 0.6834170854271356, 0.6884422110552764, 0.6934673366834171, 0.6984924623115578, 0.7035175879396985, 0.7085427135678392, 0.7135678391959799, 0.7185929648241206, 0.7236180904522613, 0.7286432160804021, 0.7336683417085427, 0.7386934673366834, 0.7437185929648241, 0.7487437185929648, 0.7537688442211056, 0.7587939698492462, 0.7638190954773869, 0.7688442211055276, 0.7738693467336684, 0.7788944723618091, 0.7839195979899497, 0.7889447236180904, 0.7939698492462312, 0.7989949748743719, 0.8040201005025126, 0.8090452261306532, 0.8140703517587939, 0.8190954773869347, 0.8241206030150754, 0.8291457286432161, 0.8341708542713567, 0.8391959798994975, 0.8442211055276382, 0.8492462311557789, 0.8542713567839196, 0.8592964824120602, 0.864321608040201, 0.8693467336683417, 0.8743718592964824, 0.8793969849246231, 0.8844221105527639, 0.8894472361809045, 0.8944723618090452, 0.8994974874371859, 0.9045226130653267, 0.9095477386934674, 0.914572864321608, 0.9195979899497487, 0.9246231155778895, 0.9296482412060302, 0.9346733668341709, 0.9396984924623115, 0.9447236180904522, 0.949748743718593, 0.9547738693467337, 0.9597989949748744, 0.964824120603015, 0.9698492462311558, 0.9748743718592965, 0.9798994974874372, 0.9849246231155779, 0.9899497487437185, 0.9949748743718593], "multi_label": true, "label_weights": [0.6129271984100342, 0.3751857280731201, 0.2630014717578888, 0.22288261353969574, 0.21619613468647003, 0.20208023488521576, 0.1552748829126358, 0.1500742882490158, 0.13595838844776154, 0.13001485168933868, 0.12704308331012726, 0.121099554002285, 0.11292719095945358, 0.10698365420103073, 0.06612183898687363, 0.06389301270246506, 0.05497771129012108, 0.051263000816106796, 0.051263000816106796, 0.04903417453169823, 0.04829123243689537, 0.045319464057683945, 0.04457652196288109, 0.04011886939406395, 0.03566121682524681, 0.034918274730443954, 0.03120356611907482, 0.03120356611907482, 0.028974739834666252, 0.02748885564506054, 0.024517087265849113, 0.020059434697031975, 0.01708766631782055, 0.016344724223017693, 0.016344724223017693, 0.014858840964734554, 0.014115898869931698, 0.014115898869931698, 0.012630014680325985, 0.011144130490720272, 0.010401188395917416, 0.010401188395917416, 0.010401188395917416, 0.00965824630111456, 0.00965824630111456, 0.008915304206311703, 0.008915304206311703, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.008172362111508846, 0.007429420482367277, 0.007429420482367277, 0.006686478387564421, 0.006686478387564421, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005943536292761564, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.005200594197958708, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.004457652103155851, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386, 0.0037147102411836386]}, "shared_object_id": 6, "build_input_shape": {"class_name": "TensorShape", "items": [null, 77]}}
(
�3"
trackable_list_wrapper
(
�3"
trackable_list_wrapper
(
�3"
trackable_list_wrapper
(
�3"
trackable_list_wrapper
(
�3"
trackable_list_wrapper
(
�0"
trackable_list_wrapper
'
*0"
trackable_list_wrapper
'
*0"
trackable_list_wrapper
�
hregularization_losses
�layer_metrics
�non_trainable_variables
�layers
itrainable_variables
�metrics
j	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
(
�0"
trackable_list_wrapper
'
+0"
trackable_list_wrapper
'
+0"
trackable_list_wrapper
�
lregularization_losses
�layer_metrics
�non_trainable_variables
�layers
mtrainable_variables
�metrics
n	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
(
�0"
trackable_list_wrapper
'
,0"
trackable_list_wrapper
'
,0"
trackable_list_wrapper
�
pregularization_losses
�layer_metrics
�non_trainable_variables
�layers
qtrainable_variables
�metrics
r	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
(
�0"
trackable_list_wrapper
'
-0"
trackable_list_wrapper
'
-0"
trackable_list_wrapper
�
tregularization_losses
�layer_metrics
�non_trainable_variables
�layers
utrainable_variables
�metrics
v	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
(
�0"
trackable_list_wrapper
'
.0"
trackable_list_wrapper
'
.0"
trackable_list_wrapper
�
xregularization_losses
�layer_metrics
�non_trainable_variables
�layers
ytrainable_variables
�metrics
z	variables
 �layer_regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_positives
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_negatives
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
#:!	�M (2true_positives
#:!	�M (2true_negatives
$:"	�M (2false_positives
$:"	�M (2false_negatives
@
�0
�1
�2
�3"
trackable_list_wrapper
.
�	variables"
_generic_user_object
#:!	�M (2true_positives
#:!	�M (2true_negatives
$:"	�M (2false_positives
$:"	�M (2false_negatives
@
�0
�1
�2
�3"
trackable_list_wrapper
.
�	variables"
_generic_user_object
#:!	�M (2true_positives
#:!	�M (2true_negatives
$:"	�M (2false_positives
$:"	�M (2false_negatives
@
�0
�1
�2
�3"
trackable_list_wrapper
.
�	variables"
_generic_user_object
#:!	�M (2true_positives
#:!	�M (2true_negatives
$:"	�M (2false_positives
$:"	�M (2false_negatives
@
�0
�1
�2
�3"
trackable_list_wrapper
.
�	variables"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
`:^2PAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/m
_:]2OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/m
_:]2OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/m
e:c
2UAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/m
d:b
2TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/m
Z:X3M2JAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/m
T:RM2HAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/m
`:^2PAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_clc/embeddings/v
_:]2OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_wr/embeddings/v
_:]2OAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_hg/embeddings/v
e:c
2UAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_parmado1/embeddings/v
d:b
2TAdam/msd_model/cat2_vec/EW_avg_embed_triangle_2_fl2_cat2vec_emb_min_top/embeddings/v
Z:X3M2JAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/kernel/v
T:RM2HAdam/msd_model/decoder/EW_avg_embed_triangle_2_fl2_decoder_output/bias/v
�2�
F__inference_msd_model_layer_call_and_return_conditional_losses_5889994�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
�2�
"__inference__wrapped_model_5889819�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
�2�
+__inference_msd_model_layer_call_fn_5890019�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
�2�
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5890225�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_cat2_vec_layer_call_fn_5890244�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
H__inference_concatenate_layer_call_and_return_conditional_losses_5890251�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
-__inference_concatenate_layer_call_fn_5890257�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
@__inference_mlp_layer_call_and_return_conditional_losses_5890261�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
%__inference_mlp_layer_call_fn_5890266�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_decoder_layer_call_and_return_conditional_losses_5890297�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_decoder_layer_call_fn_5890306�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
%__inference_signature_wrapper_5890124input_1input_2input_3input_4input_5input_6"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
__inference_loss_fn_0_5890317�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference_loss_fn_1_5890328�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference_loss_fn_2_5890339�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference_loss_fn_3_5890350�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference_loss_fn_4_5890361�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
"__inference__wrapped_model_5889819�*+,-./0���
���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
� "3�0
.
output_1"�
output_1���������M�
E__inference_cat2_vec_layer_call_and_return_conditional_losses_5890225�*+,-.���
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
"�
inputs/4���������
� "%�"
�
0���������

� �
*__inference_cat2_vec_layer_call_fn_5890244�*+,-.���
���
���
"�
inputs/0���������
"�
inputs/1���������
"�
inputs/2���������
"�
inputs/3���������
"�
inputs/4���������
� "����������
�
H__inference_concatenate_layer_call_and_return_conditional_losses_5890251�Z�W
P�M
K�H
"�
inputs/0���������)
"�
inputs/1���������

� "%�"
�
0���������3
� �
-__inference_concatenate_layer_call_fn_5890257vZ�W
P�M
K�H
"�
inputs/0���������)
"�
inputs/1���������

� "����������3�
D__inference_decoder_layer_call_and_return_conditional_losses_5890297\/0/�,
%�"
 �
inputs���������3
� "%�"
�
0���������M
� |
)__inference_decoder_layer_call_fn_5890306O/0/�,
%�"
 �
inputs���������3
� "����������M<
__inference_loss_fn_0_5890317*�

� 
� "� <
__inference_loss_fn_1_5890328+�

� 
� "� <
__inference_loss_fn_2_5890339,�

� 
� "� <
__inference_loss_fn_3_5890350-�

� 
� "� <
__inference_loss_fn_4_5890361.�

� 
� "� �
@__inference_mlp_layer_call_and_return_conditional_losses_5890261X/�,
%�"
 �
inputs���������3
� "%�"
�
0���������3
� t
%__inference_mlp_layer_call_fn_5890266K/�,
%�"
 �
inputs���������3
� "����������3�
F__inference_msd_model_layer_call_and_return_conditional_losses_5889994�*+,-./0���
���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
� "%�"
�
0���������M
� �
+__inference_msd_model_layer_call_fn_5890019�*+,-./0���
���
���
!�
input_1���������)
!�
input_2���������
!�
input_3���������
!�
input_4���������
!�
input_5���������
!�
input_6���������
� "����������M�
%__inference_signature_wrapper_5890124�*+,-./0���
� 
���
,
input_1!�
input_1���������)
,
input_2!�
input_2���������
,
input_3!�
input_3���������
,
input_4!�
input_4���������
,
input_5!�
input_5���������
,
input_6!�
input_6���������"3�0
.
output_1"�
output_1���������M