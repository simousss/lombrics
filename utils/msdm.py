from utils.learning_blocks import *
from utils.callbacks import *
from utils.custom_evals import *
from focal_loss import BinaryFocalLoss
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.losses import Reduction
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
from scipy.stats import mode
from scipy.special import logit

msdm_losses = {
     ### Probabilistic loss
    'nll': lambda w: negll_loss,
    
     ### Close-form formula
    'bce' : lambda w: BinaryCrossentropy(),
    
     ### Class and label weighted CE
    'cwbce' : lambda w: clwbce(lw=None, bw=w, avg=True, m=len(w)),
    'lwbce' : lambda w: clwbce(lw=w, bw=None, avg=True, m=len(w)),
    
     ### Focal loss
    'fl2': lambda w: BinaryFocalLoss(gamma=2), 
    'wfl2': lambda w: BinaryFocalLoss(gamma=2, pos_weight=w),
    'fl5': lambda w: BinaryFocalLoss(gamma=5),
    'wfl5': lambda w: BinaryFocalLoss(gamma=5, pos_weight=w),
    
     ### Richness-regularized 
    'rich' : lambda lfct: rich_reg_loss(lfct)
}


msdm_metrics = {
    'precision': lambda x: tfk.metrics.Precision(thresholds=x),
    'recall': lambda x: tfk.metrics.Recall(thresholds=x),
    'balacc':lambda x: balanced_accuracy,
    'fbeta': lambda x: fscore(beta_=x),
    'tss': lambda x: true_skill_stat,
    'rocauc': lambda w: tfk.metrics.AUC(curve="ROC", name='rocauc' if w is None else 'wrocauc', multi_label = True, label_weights=w),
    'prauc': lambda w: tfk.metrics.AUC(curve="PR", name='prauc' if w is None else 'wprauc', multi_label = True, label_weights=w),
    'mcc': lambda x: MatthewsCorrelationCoefficient(th = x[0], num_classes=x[1])

}  


def get_all_metrics(w, m, training=True):
    if w is None:
        w=np.ones(m)
    
    l_metric_fcts = {
        'precision': msdm_metrics.get('precision')(0.5),
        'recall': msdm_metrics.get('recall')(0.5),
        'rocauc': msdm_metrics.get('rocauc')(None),
        'prauc': msdm_metrics.get('prauc')(None),
        'wrocauc': msdm_metrics.get('rocauc')(w),
        'wprauc': msdm_metrics.get('prauc')(w)
    }
    
    if training==False:
        l_metric_fcts.update({
            'balacc': msdm_metrics.get('balacc')(0.5),
            'microf1': msdm_metrics.get('fbeta')(1.0),
#             'macrof1': msdm_metrics.get('fbeta')((0.5,1.0,'macro',m)),
#             'macrof1': msdm_metrics.get('fbeta')((0.5,1.0,'weighted',m)),    
            'microf2': msdm_metrics.get('fbeta')(0.5),
#             'macrof2': msdm_metrics.get('fbeta')((0.5,2.0,'macro',m)),
#             'macrof2': msdm_metrics.get('fbeta')((0.5,2.0,'weighted',m)),
            'mcc': msdm_metrics.get('mcc')((0.5,m)),#,
            'tss': msdm_metrics.get('tss')(0.5)
        })
    
    return l_metric_fcts



eps=1E-6
def compute_multilabel_weights(prevs):
    m=len(prevs)
    bw=((1-prevs)/(prevs+eps)).reshape((1,m))
    lw=(-np.log(prevs+eps)).reshape((1,m))  ###- log prevs = log (1/prevs)
    
    return bw, lw


def generate_archi(p, m, depth):
    archi=[]
    for i in range(depth):
        nn=p + (i+1)*(m-p)//(depth+1)
        nn_ = np.power(2,np.floor(np.log2(nn)))
        archi.append(nn_)
    
    return dict(archi=archi, reg=None, activation=['relu']*depth)


eps = 1E-6
def get_bias_init(prevs):
    init_val=logit(prevs) + eps   ### p vector of class proportions in the right order
    def my_bias_init(shape, dtype=None):
        return np.array(init_val).reshape(shape)
    
    return my_bias_init
    
    
def_accept_thr = {
    'precision': 0.0, 
    'recall': 0.0, 
    'balacc': 0.0, 
    'mcc' : 0.0, 
    'tss': 0.0,
    'microf1':0.0,
    'microf2':0.0,
    'rocauc': 0.0, 
    'prauc' : 0.0,
    'wrocauc': 0.0, 
    'wprauc' : 0.0,
    'coverage' : 0.0,
    'lr' : 0.0,
    'lrap' : 0.0
}

def_vote_weights = {
    'precision': 0.0, 
    'recall': 0.0, 
    'balacc': 0.0, 
    'mcc' : 0.0, 
    'tss': 0.0, 
    'microf1':0.0,
    'microf2':0.0,    
    'rocauc': 1.0, 
    'prauc' : 0.0,
    'wrocauc': 2.0, 
    'wprauc' : 0.0,
    'coverage' : 0.0,
    'lr' : 0.0,
    'lrap' : 0.0    
}

def select_best_config(avg_perfs, accept_thr):
    query = ' & '.join(['%s>=%f'%(k,accept_thr.get(k)) for k in accept_thr.keys()])
    filtered = avg_perfs.query(query)
    
    if filtered.shape[0]==0:
        print('No config acceptable at chosen thresholds. Consider using less severe thresholds or increase model training time.')
        result = {k:'none' for k in accept_thr.keys()}
        fail = True
    else:
        result = filtered.idxmax().to_dict()  
        fail = False
        
    return result, fail
        

def grid_search_summary(test_perfs, accept_thr, vote_weights):
    
    ### Select best config based on cross-validation performances
    metrics = [c for c in test_perfs.columns if c not in ['conf','cvblock']]
    avg_perfs = test_perfs[['conf'] + metrics].groupby('conf').mean()
    
    grid_select, fail = select_best_config(avg_perfs, accept_thr)
    
    if fail:
        voted = -1
        ballot = []
    
    else:
        ### Voting 
        ballot= []
        for k in metrics:
            ballot += [grid_select.get(k)]*int(vote_weights.get(k))

        voted = int(mode(ballot).mode[0].split('grid')[1])
    
    return grid_select, ballot, voted    
    
    
class MSDModel(tfk.Model):
    
    def __init__(self, problem, archi):
        super(MSDModel, self).__init__()
        
        ### Problem setting
        self.model_name = problem.get('name')
        self.ntaxa = problem.get('ntaxa')
        self.numvars = problem.get('numvars')
        self.catvars = problem.get('catvars')
        
        ### Architecture configuration
        self.archi = archi
        
        ### Embedding layer
        if len(self.catvars)>0:
            self.cat2vec = Cat2Vec(name=self.model_name, embed_config = self.archi.get('embed'))
            self.fconcat = tfkl.Concatenate(axis=-1)
            
        else:
            self.cat2vec = None
            
        ### Feature extraction
        self.fe = MLP(name='%s_fe'%self.model_name, mlp_config = self.archi.get('fe'))
        
        ### Response
        decoder_config={
            'ntasks':self.ntaxa,
            'dist':self.archi.get('dist'),
            'activation':None,
            'mlp': {
                'archi': [],
                'reg' : None,
                'activation' : []
            }
        }
        
        self.mode = archi.get('mode')
        if self.mode=='p':
            self.response = ProbaDecoder(name=self.model_name, decoder_config = decoder_config, 
                                         init_bias=self.archi.get('bias'))
        else:
            self.response = Decoder(name=self.model_name, decoder_config = decoder_config, init_bias=self.archi.get('bias'))
        
    
    def call(self, inputs):
        ### All features
        if len(self.catvars)>0:
            feats=[inputs[0]]
            emb = self.cat2vec(inputs[1:])
            feats.append(emb)
            feats = self.fconcat(feats)
        
        else:
            feats = inputs
             
        embed = self.fe(feats)
        output = self.response(embed)

        return output
    
    
from sklearn.utils import class_weight

def_training_params = {'epoch':200, 'batch':64, 'optimizer':'adam', 
                       'loss': 'bce', 'weighted':'binary','rich':False, 
                       'title':'training'}

class MSDMfit(object):
    def __init__(self, problem, configs=None):
        super(MSDMfit, self).__init__()
        
        ### Setting problems characteristics
        self.problem = problem
        
        ### Either we provide the configurations to be tested or we use the default generator
        self.configs = configs
        
        
    def generate_archi(self, max_depth=2):
        ### Generate potential architectures
        self.configs = []
        
        ## Generate potential embedding configurations
        if len(self.problem.get('catvars'))==0:
            l_embed=[None]
        else:
            catvars = self.problem.get('catvars')
            catvocab = self.problem.get('catclasses')
            l_embed=[dict(features=[(f,len(catvocab.get(f)),e,catvocab.get(f)) for f in catvars], reg='l2') for e in [2]]
        
        m = 1
        pnum=len(self.problem.get('numvars'))
        pcat=len(self.problem.get('catvars'))*2
        p = pnum + pcat
        
        ## Generate potential feature extraction configurations
        ## We test: 0=linear (on raw features), 1,2..max_depth hidden layers
        ### Here, we generate the number of neurons for each hidden layer given input and output params.
        l_fe = [generate_archi(p, m, depth) for depth in range(max_depth)]
        
        ## Bias init strategies
        l_bias = ['zeros','prevs']
        
        ## Decoder mode and distribution choice
        #l_modedist = [('d','logit'), ('d','probit'), ('p','bernoulli')]
        l_modedist = [('d','logit'), ('d','probit')]
        
        for i, (emb_conf, layers, bias, (mode, dist)) in enumerate(itertools.product(l_embed,l_fe,l_bias,l_modedist)): 
            conf = dict(name='%s_grid%d'%(self.problem['name'],i),embed=emb_conf,fe=layers,bias=bias,mode=mode,dist=dist)
            self.configs.append(conf)     
        
    
    def train_msdm(self, msdm, training_params, trainset, plot=False, trackset=None, taxlist=None):
        ### Function to train the model on a single dataset
        
        ## First, we get the training and validation data
        X_train, Y_train, X_valid, Y_valid = trainset
        
        ### Next, we compute label weights if the loss should be weighted
        m = Y_train.shape[0]
        prevs = Y_train.mean(axis=0)
        binw, taskw = compute_multilabel_weights(prevs)
            
        if training_params['weighted']=='binary':
            w = binw     
        elif training_params['weighted']=='task':
            w = taskw    
        else:
            w = None
        
        ### Select the loss function
        loss_name = training_params['loss']
        loss_fct = msdm_losses.get(loss_name)(w)
        
        if training_params['rich']:
            loss_fct = msdm_losses.get('rich')(loss_fct)
        
        ### Compose all relevant metrics       
        l_metrics = get_all_metrics(prevs, m, training=True)
        
        ### Compile the model
        #opt = tfk.optimizers.Adam()
        #print('loss ',loss_fct)
        msdm.compile(optimizer = training_params.get('optimizer'), loss= loss_fct, metrics= list(l_metrics.values())) 
        
        #msdm.compile(optimizer = 'adam', loss= 'binary_crossentropy', metrics= ['binary_accuracy'])
        
        ### Setup the callbacks
        cbks = [
            tfk.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, restore_best_weights=True),
            tfk.callbacks.ReduceLROnPlateau(),
            tfk.callbacks.TerminateOnNaN(),
            MlRankMetrics(valid_dataset=(X_valid, Y_valid)),
            InspectNN(period=10, model=msdm)
        ]
        
        if trackset is not None:
            prtr=PredTracker(trackset, taxlist)
            cbks.append(prtr)
        
        ###
        history = msdm.fit(x=X_train, y=Y_train, 
                           validation_data=(X_valid, Y_valid),
                           #validation_split=0.1,
                           batch_size=training_params['batch'], 
                           epochs=training_params['epoch'],
                           verbose = training_params['verbose'],
                           callbacks = cbks
                          )
        
        ### Plot learning curves
        if plot:
            plot_learning_curves(history, title=training_params.get('title'), outfile='%s'%training_params.get('name'))
        
        return history, cbks, msdm
        
    
    def eval_msdm(self, msdm, testset, binw=None, taskw=None):
        X_test, Y_test = testset
        Y_pred = msdm.predict(x=X_test)
        
        ### Compute weights
        prevs = Y_test.mean(axis=0)
        
        if (binw is None) | (taskw is None):
            binw, taskw = compute_multilabel_weights(prevs)
        
        eval_fct = get_all_metrics(prevs, Y_test.shape[1], training=False)
        
        eval_perfs = {f: eval_fct.get(f)(Y_test,Y_pred).numpy() for f in eval_fct.keys()}
        
        return eval_perfs
        
    
    def grid_search(self, cv_dataset, train_params=None, vote_weights=None, accept_thr=None):  
        ### Runs for each setting of the training parameters / 
        ### the different architectures ought to be compared using identical training conditions and datasets
        if self.configs is None:
            self.generate_archi()
        
        self.vote_weights = def_vote_weights if vote_weights is None else vote_weights
        self.accept_thr = def_accept_thr if accept_thr is None else accept_thr
        
        ### Use default parameters
        training_params = def_training_params.copy()
        training_params.update(train_params)
            
        cv_results=[]
        cv_history=[]
        
        print('Running grid search for %s'%self.problem['name'])
        
        for conf in self.configs:
            print('Fitting on architecture %s'%conf['name'])
            
            ### Update a few configs
            self.problem['name'] = conf['name']
            
            for k, (x_train, y_train, x_valid, y_valid) in enumerate(cv_dataset):
                print('Training on cross-validation fold %d'%k)
                
                trainset = (x_train, y_train, x_valid, y_valid)
                testset = (x_valid, y_valid)
                
                training_params['title'] = '%s_cv%d'%(conf['name'], k)
                
                ### Clear session and graph
                tfk.backend.clear_session()
                
                ### Init bias
                if conf['bias']=='prevs':
                    conf['bias']=get_bias_init(prevs=y_train.mean(axis=0))
                
                ### Create model
                msdm = MSDModel(problem = self.problem, archi = conf)
                
                #tfk.utils.plot_model(msdm)
                
                history, cbks, msdm = self.train_msdm(msdm,training_params,trainset, plot=False)
                perfs = self.eval_msdm(msdm,testset)
                rank_perfs = cbks[3].best_perf
                perfs.update(rank_perfs)
                perfs.update({'conf':conf['name'],'cvblock':k})
                cv_history.append(history.history)
                cv_results.append(perfs)
                del msdm
                
                
        all_results = pd.DataFrame.from_dict(cv_results)
        summary = grid_search_summary(all_results, accept_thr=self.accept_thr, vote_weights=self.vote_weights)
        
        best_conf = self.configs[summary[-1]] if summary[-1]!=-1 else None
        
        return all_results, cv_history, summary, best_conf
               