import tensorflow as tf
import tensorflow.keras as tfk
from sklearn.metrics import coverage_error, label_ranking_loss, label_ranking_average_precision_score
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def plot_learning_curves(history, title='training', outfile=None): ##TODO
    for k in history.keys():
        fig, ax = plt.subplots(1,1, figsize=(10,10))
        sns.lineplot(x=np.arange(1,len(history.get(k))+1), y=history.get(k), ax=ax)
        ax.set(xlabel='epoch', ylabel=k)

class PredTracker(tfk.callbacks.Callback):
    ### Occurrence-only dataset + environmental predictors to track predictions across epochs
    def __init__(self, trackset, taxlist):
        super(PredTracker, self).__init__()
        self.Xtrack, self.Ytrack = trackset
        self.taxlist = taxlist
        self.results = []
        self.scores = []
        
    def on_epoch_end(self, epoch, logs=None):
        ### Predict on full validation set
        Yeval_pred=pd.DataFrame(self.model.predict(self.Xtrack), columns=self.taxlist)
        
        y_pred=pd.DataFrame(data=np.array([Yeval_pred.loc[i,x] for i,x in enumerate(self.Ytrack['taxcode'])]),columns=['prob'])
        y_pred['class']=(y_pred['prob']>0.5).astype(int)
        y_pred['epoch']=epoch
        self.results.append(y_pred)
        
        recall = y_pred['class'].mean()
        entropy = y_pred['prob'].mean()
        self.scores.append({'epoch':epoch, 'recall_eval': recall, 'entropy': entropy})
    
    def on_train_end(self, logs=None):
        ### plot the evolution of metrics across epochs
        self.scores_df = pd.DataFrame.from_dict(self.scores)
        
        self.pred_df = pd.concat(self.results, axis=0)
                
class MlRankMetrics(tfk.callbacks.Callback):
    def __init__(self, valid_dataset, plot=False, outfile=None):
        super(MlRankMetrics, self).__init__()
        self.results = [] 
        self.X_valid, self.Y_valid = valid_dataset
        self.outfile = outfile
        self.plot = plot
        
    def on_epoch_end(self, epoch, logs=None):
        ### Predict on full validation set
        Y_pred=self.model.predict(self.X_valid)
        
        ### Evaluate 
        cov = coverage_error(self.Y_valid, Y_pred)
        lr = label_ranking_loss(self.Y_valid, Y_pred)
        lrap = label_ranking_average_precision_score(self.Y_valid, Y_pred)
        
        ### Save
        self.results.append({'epoch': epoch, 'coverage': cov, 'ranking_loss': lr, 'lr_avg_precision':lrap})
     
    
    def on_train_end(self, logs=None):
        ### plot the evolution of metrics across epochs
        self.results_df = pd.DataFrame.from_dict(self.results)
        
        ### Perfs on best model
        ### Predict on full validation set
        Y_pred=self.model.predict(self.X_valid)
        
        ### Evaluate 
        self.best_perf=dict(
            coverage = coverage_error(self.Y_valid, Y_pred),
            lr = label_ranking_loss(self.Y_valid, Y_pred),
            lrap = label_ranking_average_precision_score(self.Y_valid, Y_pred)
        )
        
        if self.plot:
            self.plot_mlrank()
            
    def plot_mlrank(self):
        fig, ax = plt.subplots(1,3, figsize=(15,5))
        self.results_df.plot(x='epoch',y='coverage', ax=ax[0])
        self.results_df.plot(x='epoch',y='ranking_loss', ax=ax[1])
        self.results_df.plot(x='epoch',y='lr_avg_precision', ax=ax[2])

        if self.outfile is not None:
            fig.savefig(self.outfile)        
        

        

import seaborn as sns
import matplotlib.pyplot as plt

class InspectNN(tfk.callbacks.Callback):
    def __init__(self, period=10, plot=False, model=None):
        super(InspectNN, self).__init__()
        self.msdm = model
        self.period = period
        self.embed_norms=[]
        self.weight_norms=[]
        
        self.plot = plot
        
        if self.msdm.cat2vec is not None:
            self.categs=[]
            self.featnames=[]
            for f in self.msdm.cat2vec.config['features']:
                self.categs+=f[-1]
                self.featnames+= ([f[0]] * f[1])
        
    def on_epoch_end(self, epoch, logs=None):
        if epoch % self.period == 0:
            ## Compute the norm of the embedding for each class of each categorical feature
            c2v = self.msdm.cat2vec
            
            if c2v is not None:
                embed_norm=pd.DataFrame(np.concatenate([np.linalg.norm(w, axis=1) for w in c2v.get_weights()],axis=0),
                                        columns=['emb_norm'])
                embed_norm['feature'] = self.featnames
                embed_norm['category'] = self.categs
                embed_norm['epoch'] = epoch
                
                self.embed_norms.append(embed_norm)
            
            ## Compute the norm of the weights at each FE neuron
            fe = self.msdm.fe
            norms=[('%s'%('w' if i%2==0 else 'b') , i//2 +1 , np.linalg.norm(w)) for i, w in enumerate(fe.get_weights())]
            fe_depth = len(norms)/2
            
            ## Add the norm of decoder weights
            dec = self.msdm.response
            rnorms=[('%s'%('rw' if i%2==0 else 'rb') , i//2 + fe_depth+1 , np.linalg.norm(w)) 
                    for i, w in enumerate(dec.get_weights())]
            
            df = pd.DataFrame(np.array(norms + rnorms), columns=['type','depth','norm'])
            df['epoch']=epoch
            
            self.weight_norms.append(df)
        
    
    def on_train_end(self, logs=None):
        if self.plot:
            self.plot_inspect()
        
    def plot_inspect(self):
        ### plot the evolution of weight norms across epochs
        
        if len(self.embed_norms)>0:
            self.all_embed_norms = pd.concat(self.embed_norms,axis=0)
            del self.embed_norms

            ### Visualize
            catfeat = self.all_embed_norms['feature'].unique()
            for c in catfeat:
                fig, ax = plt.subplots(1,1,figsize=(5,5))
                sns.lineplot(x='epoch', y='emb_norm', hue='category', ax=ax, 
                             data=self.all_embed_norms.query('feature==@c')).set_title('Embedding norms %s'%c)         

        ### plot the evolution of embedding norms across epochs
        self.all_neuron_norms = pd.concat(self.weight_norms, axis=0)
        del self.weight_norms
        
        ### Visualize FE and response neurons
        fig, ax = plt.subplots(1,1,figsize=(5,5))
        sns.lineplot(x='epoch',y='norm',hue='depth',style='type', ax=ax, data=self.all_neuron_norms).set_title(
            'Neuron weight norms')
        
                