from utils.learning_blocks import *


def mtvae_loss(y_true,y_pred):
    return -y_pred.log_prob(y_true)

def rich_loss(y_true, y_pred):    
    negll=-tf.reduce_mean(y_pred.log_prob(y_true))
    
    y_true = tf.cast(y_true, dtype=tf.float32)
    rich_true = tf.reduce_sum(y_true,axis=-1)
    rich_pred = tf.reduce_sum(y_pred,axis=-1)
    richll=tf.reduce_sum(tf.math.exp(rich_true - rich_pred))
    
    return negll + richll


def dirich_multi_loss(y_true, y_pred):
    total = tf.reduce_sum(y_true, axis=-1)
    pred_dist = tfd.DirichletMultinomial(
                    total_count=total,
                    concentration=y_pred)
    
    return -pred_dist.log_prob(y_true)


class MTVAE(tfk.Model):
    def __init__(self, fe_config, encoder_config, decoder_config, name='mtvae'):
        
        super(MTVAE, self).__init__()
        
        ### Model name
        self.model_name = name
        
        ### Feature Representation 
        self.has_num=fe_config.get('num_vars') is not None
        self.has_cat=fe_config.get('cat_vars') is not None
        
        if fe_config.get('embed') is not None:
            self.cat2vec = Cat2Vec(fe_config.get('embed'),self.model_name)
            
        self.rawfeat = tfkl.Concatenate(name='%s_rawfeat')
        
        ### Feature extraction
        self.fext = MLP(fe_config.get('fe'),name='%s_fe'%self.model_name)
        
        ### Latent factors
        self.encoder = GaussianEncoder(encoder_config, self.model_name)
        
        ### Input to the decoder
        self.allfeat = tfkl.Concatenate(name='%s_allfeat')
        
        ### Decoder
        if decoder_config.get('type')=='deterministic':
            if type(decoder_config.get('config'))==list:
                self.decoder=MultiDecoder(decoder_config.get('config'), 
                                          tnames=decoder_config.get('tnames'),name=self.model_name)
            else:
                self.decoder=Decoder(decoder_config.get('config'), name=self.model_name)
        else:
            if type(decoder_config.get('config'))==list:
                self.decoder=ProbaMultiDecoder(decoder_config.get('config'), 
                                               tnames=decoder_config.get('tnames'), name=self.model_name)
            else:
                self.decoder=ProbaDecoder(decoder_config.get('config'), name=self.model_name)
    
    def call(self, inputs):
        ### Acquire inputs
        num_feats = inputs[0:1] if self.has_num else []
        cat_feats = self.cat2vec(inputs[1:-1]) if self.has_cat else []
        raw_feats = self.rawfeat(num_feats+cat_feats)
        
        ### Sample latent factors
        lfactors=self.encoder(inputs[-1])
        
        ### FE
        hidden = self.fext(raw_feats)
        hidden = self.allfeat([hidden, lfactors])
            
        
        output = self.decoder(hidden)
        
        return output
        
        