### Parsing dates in the dd/mm/yyyy format
def parse_date(x):
    res=x.split('/')[-1] if x==x else x
    try:
        res=int(res)
    except ValueError:
        res=0
    
    return res


### Encode taxa name
def encode_ewtaxa(vec):
    gen=vec['Genus'][0:2]
    spec=vec['Species'][0:4]
    code='%s%s'%(gen,spec)
    if vec['Sub_species']==vec['Sub_species']:
        subs=vec['Sub_species'][0:3]
        code='%s_%s'%(code,subs)
    
    if vec['Variety']==vec['Variety']:
        vari=''.join([x[0:3] for x in vec['Variety'].split(' ')])
        code='%s_%s'%(code,vari)
        
    
    return code

### Encode species name
def encode_ewspec(vec):
    code='%s%s'%(vec['Genus'][0:2],vec['Species'][0:4])
    return code


### Convert coordinates from crs to another
from pyproj import Proj, CRS, Transformer
def convert_coords(crs_src,crs_dst, lon, lat):
    p1=CRS.from_user_input(crs_src)
    p2=CRS.from_user_input(crs_dst)
    
    transformer = Transformer.from_crs(crs_from=p1, crs_to=p2)
    lat_, lon_ = transformer.transform(yy=lon, xx=lat)
    
    return lon_, lat_
# x, y = convert_coords(4326,3035,lon=0.3705226448469283, lat=45.40583115539892)
# x, y
# ### Expected: x, y = (3568363,2525763)
# ### Expected: x, y = (0.3705226448469283, 45.40583115539892)

def select_clc_dataset(clc_temp_scope,year):
    ### get the appropriate period
    if year<clc_temp_scope[0].get('adj_scope')[0]:
        res=0
    elif year>clc_temp_scope[-1].get('adj_scope')[1]:
        res=len(clc_temp_scope)-1
    else:
        for i, clc_period in enumerate(clc_temp_scope):
            ymin, ymax = clc_period.get('adj_scope')
            if ymin<=year<=ymax:
                res=i
                break
    
    return 'clc_%d_%d'%clc_temp_scope[res].get('adj_scope')


