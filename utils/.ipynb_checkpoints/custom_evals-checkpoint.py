import tensorflow as tf
from tensorflow_addons.metrics import FBetaScore, MatthewsCorrelationCoefficient
import numpy as np
import pandas as pd
import tensorflow.keras as tfk
from tensorflow.keras.metrics import TruePositives, TrueNegatives, FalsePositives, FalseNegatives

eps = tfk.backend.epsilon()

def negll_loss(y_true, y_pred):
    return -tf.reduce_mean(y_pred.log_prob(y_true))


def clwbce(bw=None,lw=None,avg=False, m=1):
    if bw is None:
        bw=np.ones(m)
    if lw is None:
        lw=np.ones(m)
        
    def bce_weighted(y_true,y_pred):
        # Compute cross entropy from probabilities.
        y_true=tf.cast(y_true,tf.float32)
        y_pred=tf.cast(y_pred,tf.float32)
        
        bcel = bw * y_true * tf.math.log(y_pred + eps)
        bcel += (1 - y_true) * tf.math.log(1 - y_pred + eps)
        return tf.math.reduce_mean(-bcel * lw) if avg else -bcel*lw
  
    return bce_weighted

def rich_reg_loss(loss_fct, det=True):
    ### NB: works with deterministic y_pred only 
    def rich_loss(y_true, y_pred):  
        y_true = tf.cast(y_true, dtype=tf.float32)
        y_pred = tf.cast(y_pred, dtype=tf.float32)
        
        negll=loss_fct(y_true, y_pred)
        rich_true = tf.reduce_sum(y_true,axis=-1)
        rich_pred = tf.reduce_sum(y_pred,axis=-1)
        richll=tf.reduce_mean(tf.math.exp(rich_true - rich_pred))

        return negll + richll
    
    return rich_loss #if det else prich_loss


from tensorflow_addons.metrics.multilabel_confusion_matrix import MultiLabelConfusionMatrix

def conf_mat(y_true, y_pred):
    mcm=MultiLabelConfusionMatrix(num_classes=tf.shape(y_true)[1])(y_true, y_pred)
    microcm = tf.reduce_sum(mcm, axis=0)
    
    tn = microcm[0,0]
    fp = microcm[0,1]
    fn = microcm[1,0]
    tp = microcm[1,1]
    
    return tn, fp, fn, tp

def sensitivity_score(y_true, y_pred):
    tn, fp, fn, tp = conf_mat(y_true, y_pred>0.5)
    
    sensitivity_ =  tp / (tp + fn)
    return sensitivity_

def specificity_score(y_true, y_pred):
    tn, fp, fn, tp = conf_mat(y_true, y_pred>0.5)
    
    specificity_ = tn / (tn + fp)
    return specificity_ 


def true_skill_stat(y_true, y_pred):
    tss_score = sensitivity_score(y_true, y_pred) + specificity_score(y_true,y_pred) - 1
    return tss_score

def balanced_accuracy(y_true, y_pred):
    balacc_score = (sensitivity_score(y_true, y_pred) + specificity_score(y_true,y_pred)) * 0.5
    return balacc_score


def fscore(beta_=1):
    def fbeta_score(y_true, y_pred):
        precision = tfk.metrics.Precision(thresholds=0.5)(y_true, y_pred)
        recall = tfk.metrics.Recall(thresholds=0.5)(y_true, y_pred)

        beta_weight = beta_*beta_
        fscore = (1+beta_weight)*(precision*recall)/(beta_weight*precision + recall)

        return fscore
    
    return fbeta_score


# class TrueSkillStat(tfk.metrics.Metric):
#     def __init__(self, name='tss', th=0.5, **kwargs):
#         super(TrueSkillStat, self).__init__(name=name, **kwargs)
#         self.tss = self.add_weight(name='tss', initializer='zeros')
#         self.threshold = th
        
#         self.ftp = tfk.metrics.TruePositives(thresholds=self.threshold)
#         self.ftn = tfk.metrics.TrueNegatives(thresholds=self.threshold)
#         self.ffp = tfk.metrics.FalsePositives(thresholds=self.threshold)
#         self.ffn = tfk.metrics.FalseNegatives(thresholds=self.threshold)        
    
#     def update_state(self, y_true, y_pred, sample_weight=None):
#         y_true = tf.cast(y_true, tf.bool)
#         y_pred = tf.cast(y_pred, tf.bool)
        
#         tp = self.ftp(y_true, y_pred)
#         tn = self.ftn(y_true, y_pred)
#         fp = self.ffp(y_true, y_pred)
#         fn = self.ffn(y_true, y_pred)
        
#         sensitivity =  tp / (tp + fn)
#         specificity = tn / (tn + fp)
        
#         values = sensitivity + specificity - 1
        
#         if sample_weight is not None:
#             sample_weight = tf.cast(sample_weight, self.dtype)
#             values = tf.multiply(values, sample_weight)
        
#         self.tss.assign_add(tf.reduce_mean(values))        

#     def result(self):
#         return self.tss
    
# #     def reset_state(self):
# #         self.tss.assign(0)    

        

# class BalancedAcc(tfk.metrics.Metric):
#     def __init__(self, name='balacc', th=0.5, **kwargs):
#         super(BalancedAcc, self).__init__(name=name, **kwargs)
#         self.balacc = self.add_weight(name='balacc', initializer='zeros')
#         self.threshold = th
        
#         self.ftp = tfk.metrics.TruePositives(thresholds=self.threshold)
#         self.ftn = tfk.metrics.TrueNegatives(thresholds=self.threshold)
#         self.ffp = tfk.metrics.FalsePositives(thresholds=self.threshold)
#         self.ffn = tfk.metrics.FalseNegatives(thresholds=self.threshold)        
    
#     def update_state(self, y_true, y_pred, sample_weight=None):
#         y_true = tf.cast(y_true, tf.bool)
#         y_pred = tf.cast(y_pred, tf.bool)
        
#         tp = self.ftp(y_true, y_pred)
#         tn = self.ftn(y_true, y_pred)
#         fp = self.ffp(y_true, y_pred)
#         fn = self.ffn(y_true, y_pred)       
        
#         sensitivity =  tp / (tp + fn)
#         specificity = tn / (tn + fp)
        
#         values = (sensitivity + specificity   ) * 0.5      
        
#         if sample_weight is not None:
#             sample_weight = tf.cast(sample_weight, self.dtype)
#             values = tf.multiply(values, sample_weight) 
        
#         self.balacc.assign_add(tf.reduce_mean(values))        

#     def result(self):
#         return self.balacc
    
# #     def reset_state(self):
# #         self.balacc.assign(0)
