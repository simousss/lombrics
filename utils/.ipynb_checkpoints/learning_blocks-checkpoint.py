import tensorflow as tf
import tensorflow_probability as tfp 

tfk = tf.keras
tfkl = tfk.layers
tfkr = tfk.regularizers
tfd = tfp.distributions
tfpl = tfp.layers
tfki = tfk.initializers


eps = 1E-6
def get_bias_init(prevs):
    init_val=np.log(prevs) + eps   ### p vector of class proportions in the right order
    def my_bias_init(shape, dtype=None):
        return np.array(init_val).reshape(shape)
    
    
def probit(x):
    return tfd.Normal(loc=0.,scale=1.).cdf(x)


class Cat2Vec(tfkl.Layer):
    def __init__(self, embed_config, name):
        super(Cat2Vec, self).__init__()
        
        self.config = embed_config
        self.model_name='%s_cat2vec'%name
        
        self.embed_layers=[tfkl.Embedding(input_dim = c[1], output_dim = c[2],
                           embeddings_regularizer = embed_config.get('reg'), 
                                          name='%s_emb_%s'%(self.model_name,c[0]))
                           for c in embed_config.get('features')]

    def call(self, inputs):
        embeddings = [
            self.embed_layers[i](inp) for i, inp in enumerate(inputs)
        ]
        
        output = tfkl.Concatenate()(embeddings)
        
        return tf.squeeze(output,axis=-2)
    
    

class MLP(tfkl.Layer):
    def __init__(self, mlp_config, name):
        super(MLP, self).__init__()
        
        self.model_name = name
        self.config = mlp_config
        self.layers=[]
        
        activ = mlp_config.get('activation')
        
        for i, nn in enumerate(mlp_config.get('archi')):
            if nn>=1:
                hidden=tfkl.Dense(units = int(nn), 
                           name='%s_dense_%d'%(self.model_name,i), 
                           activation = activ[i] if i<len(activ) else None, 
                           kernel_regularizer = mlp_config.get('reg'))
                
            elif nn>0:
                hidden=tfkl.Dropout(rate=nn, name='%s_dropout_%d'%(self.model_name,i))
                
            else:
                print('Error, incorrect number of neurons / dropout rate')                
            
            self.layers.append(hidden)

    def call(self, inputs):
        x=inputs
        for dlay in self.layers:
            x=dlay(x)
            
        return x
    
    
class GaussianEncoder(tfkl.Layer):

    def __init__(self, encoder_config, name):
        super(GaussianEncoder, self).__init__()
        
        ### Model name
        self.model_name='%s_encoder'%name
        
        ### Config
        self.config=encoder_config
        mlp = encoder_config.get('mlp')
        latentdim = encoder_config.get('latentdim')
        
        ### Adjusting encoder output to distribution shape
        mlp.get('archi').append(tfpl.MultivariateNormalTriL.params_size(latentdim))            
        
        ### Encoding MLP layer
        self.encoder = MLP(mlp_config=mlp,name=self.model_name)
        
        ### posterior estimation params
        beta = encoder_config.get('beta')
        nsamples = encoder_config.get('nsamples')

        ### Default prior
        self.prior = tfd.MultivariateNormalDiag(loc=tf.zeros(latentdim),scale_diag=tf.ones(latentdim))

        ### Regularizer
        self.kl_regularizer = tfpl.KLDivergenceRegularizer(
            self.prior,
            weight=beta,
            use_exact_kl=True,
            test_points_fn=lambda q: q.sample(nsamples),
            test_points_reduce_axis=None
        )
            
        ### Posterior sampling layer
        self.sampling = tfpl.MultivariateNormalTriL(event_size = latentdim, 
                                                   name='%s_latentsample'%self.model_name,
                                                   activity_regularizer=self.kl_regularizer)

    def call(self, inputs):
        latent_params=self.encoder(inputs)
        latent_factor=self.sampling(latent_params)
        
        return latent_factor
    


class Decoder(tfkl.Layer):

    def __init__(self, decoder_config, name, init_bias="zeros"):
        super(Decoder, self).__init__()
        
        ### Model name
        self.model_name='%s_decoder'%name
        
        ### Config
        self.config=decoder_config
        ntasks = decoder_config.get('ntasks')
        
        ### Logit model
        self.fe = MLP(mlp_config = decoder_config.get('mlp'), name=self.model_name)
        
        ### Response model
        if self.config['dist']=='gaussian':
            activation = 'linear'
        
        elif self.config['dist']=='apoisson':
            activation = 'softplus'
            
        elif self.config['dist']=='poisson':
            activation = 'exponential'
        
        if self.config['dist']=='categorical':
            activation = 'softmax'
            
        if self.config['dist']=='probit':
            activation = probit
            
        elif self.config['dist']=='logit':
            activation = 'sigmoid'
            
        else:
            activation = self.config['activation']
         
        self.prediction = tfkl.Dense(units=ntasks,activation=activation, 
                                     bias_initializer=init_bias,
                                     name='%s_output'%self.model_name)
        

    def call(self, inputs):
        features = self.fe(inputs)
        output = self.prediction(features)
        
        return output
    

class ProbaDecoder(tfkl.Layer):

    def __init__(self, decoder_config, name, init_bias="zeros"):
        super(ProbaDecoder, self).__init__()
        
        ### Model name
        self.model_name='%s_pdecoder'%name
        
        ### Config
        self.config=decoder_config
        ntasks = decoder_config.get('ntasks')
        
        ### Logit model
        self.fe = MLP(mlp_config = decoder_config.get('mlp'), name=self.model_name)
        
        ### Response model
        if self.config['dist']=='independent':
            activation = 'linear'
            outdist = tfpl.IndependentNormal
            
        if self.config['dist']=='gaussian':
            activation = 'linear'
            outdist = tfpl.MultivariateNormalTriL       
        
        elif self.config['dist']=='apoisson':
            activation = 'softplus'
            outdist = tfpl.IndependentPoisson
            
        elif self.config['dist']=='poisson':
            activation = 'exponential'
            outdist = tfpl.IndependentPoisson
        
        if self.config['dist']=='categorical':
            activation = 'softmax'
            outdist = tfpl.OneHotCategorical
            
        if self.config['dist']=='bernoulli':
            activation = 'linear'
            outdist = tfpl.IndependentBernoulli
            
        
        nparams = outdist.params_size(ntasks)
        self.pred_params = tfkl.Dense(units=nparams,activation=activation,
                                     bias_initializer=init_bias,
                                     name='%s_outparams'%self.model_name)
        
        
        self.prediction = outdist(ntasks, name='%s_outdist'%self.model_name)

    def call(self, inputs):
        features = self.fe(inputs)
        outparams = self.pred_params(features)
        output = self.prediction(outparams)
        
        return output
    
    
class MultiDecoder(tfkl.Layer):
    
    def __init__(self, decoder_configs, tnames, name):
        super(MultiDecoder, self).__init__()
        
        ## Multi_decoder
        self.model_name = '%s_mtdecoder'%name
        self.task_names = tnames
        
        self.task_decoders=[]
        for t, conf in enumerate(decoder_configs):
            task = Decoder(decoder_config = conf, name='%s_%s_decoder'%(name,tnames[t]))
            self.task_decoders.append(task)
            
    
    def call(self, inputs):
        outputs = [decoder(inputs) for decoder in self.task_decoders]
        
        return outputs
            

class ProbaMultiDecoder(tfkl.Layer):
    
    def __init__(self, decoder_configs, tnames, name):
        super(ProbaMultiDecoder, self).__init__()
        
        ## Multi_decoder
        self.model_name = '%s_pmtdecoder'%name
        self.task_names = tnames
        
        self.task_pdecoders=[]
        for t, conf in enumerate(decoder_configs):
            task = ProbaDecoder(decoder_config = conf, name='%s_%s_pdecoder'%(name,tnames[t]))
            self.task_pdecoders.append(task)
            
    
    def call(self, inputs):
        outputs = [pdecoder(inputs) for pdecoder in self.task_pdecoders]
        
        return outputs
            
        
    
    


