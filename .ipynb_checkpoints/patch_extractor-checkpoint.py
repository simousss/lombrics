'''

'''

from osgeo import gdal
import rasterio 
from rasterio.fill import fillnodata
from pyproj import Proj, transform
import numpy as np
import scipy.stats as ss

## Metadata object:
### {resolution, width, height, crs}

# def patch_freqs(ref_dict):
#     def aggregate(arr):
#         base=ref_dict.copy()
#         freqs=np.unique(arr,return_counts=True)
#         base.update(dict(zip(freqs[0].data,freqs[1])))
        
#         return np.array(list(base.values()))
         
#     return aggregate


def read_raster(file, nodata,smooth=0, interpolate=False):
    ## Read raster
    rst=rasterio.open(file).read(1)
    
    if interpolate:
        mask=(rst==nodata)
    
        ## Spatial interpolation 
        rst_fill=fillnodata(rst,mask=mask,max_search_distance=100,smoothing_iterations=smooth)# if dtype=='int' else 20)
    
        ## Return raster data
        return rst_fill
    
    return rst
    

class PatchExtractor(object):
    def __init__(self,name='',metadata=None):
        self.name=name
        self.raster_files=[]
        self.metadata=metadata
        self.data=None
        
    def create_stack(self,raster_list,features):
        self.raster_files = raster_list
        self.features=features
        
#         outvrt = '%s.vrt'%self.name #/vsimem is special in-memory virtual "directory"
#         outtif = '%s.tif'%self.name

#         outds = gdal.BuildVRT(outvrt, raster_list, separate=True)
#         self.outds = gdal.Translate(outtif, outds)
        
        ## Read each file in the list
        rasters=[read_raster(rf,self.metadata['nodata']) for rf in self.raster_files]
        
        #self.data = outds.ReadAsArray().astype(self.metadata['dtype'])  ## reads numpy array of rank 3   [nb_features, width, height]
        
        nband=len(self.raster_files)
        if(nband>1):
            self.data = np.stack(rasters,axis=2)
        else:
            self.data=np.expand_dims(rasters[0],axis=2)
           
        self.data=self.data.astype(self.metadata['dtype'])
        
        ## Set NODATA to NA
        repl = self.metadata['nodata'] if self.data.dtype in [np.int16, np.int32, np.int64] else np.nan 
        #np.float32, np.float64] else np.PINF
        self.data[np.where(self.data==self.metadata['nodata'])] = repl
        
        #self.data=np.ma.masked_invalid(np.ma.masked_equal(self.data,self.metadata['nodata'],copy=True))
        
        if(self.data.dtype in [np.int32]):
            ## considered as categorical
            self.categories=np.unique(self.data).tolist()
            #self.ref_count=dict(zip(self.categories,[0]*len(self.categories)))
            
            # One hot encode raster
            #self.rawdata = self.data.copy()
            #self.data = np.array([(self.data.squeeze(axis=-1) == i) for i in self.categories]).transpose((1,2,0)).astype(np.int32)
            
            #self.data=np.nan_to_num(self.data).astype(np.int32)
            
        
        ## Update metadata
        self.metadata['width']=self.data.shape[0]
        self.metadata['height']=self.data.shape[1]
        self.metadata['nbands']=self.data.shape[2] 
        self.metadata['dtype']=self.data.dtype
    
    def get_coordinates(self,sample,lon,lat,crs='epsg:4326'):
        ##convert to the right crs
        if crs!=self.metadata['crs']:
            p1 = Proj(crs)
            p2 = Proj(self.metadata['crs'])
            lat, lon = transform(p1, p2, lat, lon)
        
        ## Compute the pixel coordinates from the geographic coordinates
        raster=rasterio.open(self.raster_files[0])
        x, y=raster.index(x=lon,y=lat)
        
        ## verify coords
        xmax=self.metadata['width']
        ymax=self.metadata['height']
        
        out_extent=[i for i in range(len(sample)) if ((x[i]<0) or (x[i]>=xmax) or (y[i]<0) or (y[i]>=ymax)) ]
        if (len(out_extent)>0): 
            print('Warning: %d samples out of raster extent. Check "out_extent" object'%len(out_extent))
            x=[xi for i,xi in enumerate(x) if i not in out_extent]
            y=[yi for i,yi in enumerate(y) if i not in out_extent]
        
        return x, y, out_extent
    
    def extract_patch(self,coords,distance=10000,mode='patch',num_fct=np.nanmedian,cat_fct='mode'):
        
        vnames = self.features.copy() #['%s_%s'%(self.features[0],c) for c in self.categories] if self.metadata['dtype'] in [np.int32] else self.features.copy()
        
        if distance<=0:  ##point mode
            ## Get data at exact coordinates
            result = self.data[coords[0],coords[1],:]    
            
        else: 
            steps = distance//self.metadata['resolution']
            ## Extract patch delimited by coords
            x, y = coords
            x = np.array(x)
            y = np.array(y)
            xmin = np.maximum(0,x - steps)
            xmax = np.minimum(x + steps, self.metadata['width'])
            ymin = np.maximum(0,y - steps)
            ymax = np.minimum(y + steps, self.metadata['height'])
            
            bounds=np.stack([xmin,xmax,ymin,ymax],axis=1)
            patch=np.apply_along_axis(arr=bounds,axis=1,func1d=lambda row: self.data[row[0]:row[1], row[2]:row[3],:])
        
            ## Summarize
            if mode=='patch':
                result=patch
                
            else:
                ## aggregate
                if self.metadata['dtype'] in [np.float32,np.float64]:
                    #result=num_fct(patch,axis=tuple((1,2)))
                    result=num_fct(patch,axis=(1,2))
                    
                else:
                    ### One hot encode
                    patch = np.array([(patch.squeeze(axis=-1) == i) for i in self.categories]).transpose((1,2,3,0)).astype(np.int32)
                    ## Compute counts of labels
                    counts = np.nansum(patch,axis=(1,2))
                        
                    if cat_fct=='freqs':
                        result=counts/(4*(steps**2))
                    else:
                        result = counts
                        
                    vnames= ['%s_%s'%(self.features[0],c) for c in self.categories]
                    
                
        return result, vnames
        
        
        